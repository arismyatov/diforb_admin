﻿(function () {
    'use strict';

    angular
        .module('diforbApp')
        .factory('soundFileManagerClient', soundManagerClient);

    soundManagerClient.$inject = ['$resource'];

    function soundManagerClient($resource) {
        return $resource("api/sound/:fileName",
                { id: "@fileName" },
                {
                    'query': {method:'GET'},
                    'save': { method: 'POST', transformRequest: angular.identity, headers: { 'Content-Type': undefined } },
                    'remove':{method: 'DELETE', url: 'api/sound/:fileName', params:{name:'@fileName'}}
                });
    }
})();
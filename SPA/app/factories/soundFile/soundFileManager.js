﻿(function () {
    'use strict';

    angular
        .module('diforbApp')
        .factory('soundFileManager', soundFileManager);

    soundFileManager.$inject = ['$q', 'soundFileManagerClient', 'appInfo'];

    function soundFileManager($q, soundFileManagerClient, appInfo) {
        var service = {
            soundFiles: [],
            load: load,
            upload: upload,
            remove: remove,
            soundFileExists: soundFileExists,
            status: {
                uploading: false
            }
        };

        return service;

        function load() {
            appInfo.setInfo({busy:true, message:"loading soundFiles"})
            
            service.soundFiles.length = 0;

            return soundFileManagerClient.query()
                                .$promise
                                .then(function (result) {                                    
                                    result.soundFiles
                                            .forEach(function (soundFile) {
                                                    service.soundFiles.push(soundFile);
                                                });

                                    appInfo.setInfo({message: "soundFiles loaded successfully"});

                                    return result.$promise;
                                },
                                function (result) {
                                    appInfo.setInfo({message: "something went wrong: " + result.data.message});
                                    return $q.reject(result);
                                })                   
                                ['finally'](
                                function () {
                                    appInfo.setInfo({busy: false});
                                });
        }

        function upload(soundFiles)
        {
            service.status.uploading = true;
            appInfo.setInfo({ busy: true, message: "uploading soundFiles" });            

            var formData = new FormData();

            angular.forEach(soundFiles, function (soundFile) {
                formData.append(soundFile.name, soundFile);
            });

            return soundFileManagerClient.save(formData)
                                        .$promise
                                        .then(function (result) {
                                            if (result && result.soundFiles) {
                                                result.soundFiles.forEach(function (soundFile) {
                                                    if (!soundFileExists(soundFile.name)) {
                                                        service.soundFiles.push(soundFile);
                                                    }
                                                });
                                            }

                                            appInfo.setInfo({message: "soundFiles uploaded successfully"});

                                            return result.$promise;
                                        },
                                        function (result) {
                                            appInfo.setInfo({message: "something went wrong: " + result.data.message});
                                            return $q.reject(result);
                                        })
                                        ['finally'](
                                        function () {
                                            appInfo.setInfo({ busy: false });                                            
                                            service.status.uploading = false;
                                        });
        }

        function remove(soundFile) {
            appInfo.setInfo({ busy: true, message: "deleting soundFile " + soundFile.name });            

            return soundFileManagerClient.remove({fileName: soundFile.name})
                                        .$promise
                                        .then(function (result) {
                                            //if the soundFile was deleted successfully remove it from the soundFiles array
                                            var i = service.soundFiles.indexOf(soundFile);
                                            service.soundFiles.splice(i, 1);

                                            appInfo.setInfo({message: "soundFiles deleted"});

                                            return result.$promise;
                                        },
                                        function (result) {
                                            appInfo.setInfo({message: "something went wrong: " + result.data.message});
                                            return $q.reject(result);
                                        })
                                        ['finally'](
                                        function () {
                                            appInfo.setInfo({busy: false});
                                        });
        }

        function soundFileExists(soundFileName) {
            var res = false
            service.soundFiles.forEach(function (soundFile) {
                if (soundFile.name === soundFileName) {
                    res = true;
                }
            });

            return res;
        }
    }
})();
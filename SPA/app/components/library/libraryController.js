﻿app.controller("libraryController", function ($scope, $http, Common, LibraryService) {

        //declare variable for mainain ajax load and entry or edit mode
        $scope.message = 'Libraries';
        $scope.loading = true;
        $scope.addMode = false;
        $scope.newlibrary = {};
        $scope.apiUrl = Common.baseApiUrl + '/api/Library';

        loadAllLibraries();  

        function loadAllLibraries()  
        {  
            var promiseGetLibraries = LibraryService.getAll();             
            promiseGetLibraries.then(function (pl) { $scope.libraries = pl.data },  
                  function (errorPl) {  
                      $scope.error =  errorPl;  
                  });  
        }    

        //by pressing toggleEdit button ng-click in html, this method will be hit
        $scope.toggleEdit = function () {
            this.libraries.editMode = !this.libraries.editMode;
        };

        //by pressing toggleAdd button ng-click in html, this method will be hit
        $scope.toggleAdd = function () {
            $scope.addMode = !$scope.addMode;
        };

        $scope.save = function () {  
            var promisePost = LibraryService.post(this.newlibrary);

            promisePost.then(function (pl) {  
                alert("New Library is saved Successfully.");  
            },  
            function (errorPl) {  
              $scope.error = 'failure saving Student', errorPl;  
            });  
        };

        //Inser Customer
        $scope.add = function () {
            $scope.loading = true;
            $http.post($scope.apiUrl, this.newlibrary).success(function(data) {
                alert("Added Successfully!!");
                $scope.addMode = false;
                $scope.libraries.push(data);
                $scope.loading = false;
            }).error(function (data) {
                $scope.error = "An Error has occured while Adding Library! " + data;
                $scope.loading = false;
            });
        };

        // //Edit Customer
        // $scope.save = function () {
        //     $scope.loading = true;
        //     var lib = this.library;
        //     $http.put($scope.apiUrl + lib.Id, lib).success(function (data) {
        //         alert("Saved Successfully!!");
        //         lib.editMode = false;
        //         $scope.loading = false;
        //     }).error(function (data) {
        //         $scope.error = "An Error has occured while Saving library! " + data;
        //         $scope.loading = false;
        //     });
        // };

        //Delete Customer
        $scope.deletelibrary = function () {
            $scope.loading = true;
            var Id = this.library.Id;
            $http.delete($scope.apiUrl + Id).success(function (data) {
                alert("Deleted Successfully!!");
                $.each($scope.libraries, function (i) {
                    if ($scope.libraries[i].Id === Id) {
                        $scope.libraries.splice(i, 1);
                        return false;
                    }
                });
                $scope.loading = false;
            }).error(function (data) {
                $scope.error = "An Error has occured while Deleting Library! " + data;
                $scope.loading = false;
            });
        };
    }
);
// LibsServices
// app.service('LibraryService', function($http, GeneralSettings) {
//     this.apiUrl = GeneralSettings.baseApiUrl + '/api/Library';
//     this.loading = false;    
//     this.GetLibraries = function(libs)
//     {
// 	    $http.get(this.apiUrl).success(function (data) {
// 	        this.libraries = data;
// 	        this.loading = false;
// 	        if(libs)
// 	        {
// 	        	libs = data;
// 	        }
// 	    })    	
//     };


//     $http.get(this.apiUrl).success(function (data) {
//         this.libraries = data;
//         this.loading = false;
//     })
//   }
// );



// app.factory('LibraryService', function($http, GeneralSettings) {
//   var apiUrl = GeneralSettings.baseApiUrl + '/api/Library';
  
//   var promise;
//   var libraryService = {
//     getAll: function() {
//       if ( !promise ) {
//         // $http returns a promise, which has a then function, which also returns a promise
//         promise = $http.get(apiUrl).then(function (response) {
//           // The return value gets picked up by the then in the controller.
//           return response.data;
//         });
//       }
//       // Return the promise to the controller
//       return promise;
//     }
//   };
//   return libraryService;
// });


app.factory('LibraryService', function($http, Common) {
  var apiUrl = Common.baseApiUrl + '/api/Library'; 
  var promise;

  var libraryService = {
    getAll: function() {
      return $http.get(apiUrl); 
    },

    getLibrary: function(id) {
      return $http.get(apiUrl + '/' + id); 
    },

    post: function(library) {
        var request = $http({  
            method: "post",  
            url: apiUrl,  
            data: library  
        });  
        return request; 
    },

    put: function(id, library) {
        var request = $http({  
            method: "put",  
            url: apiUrl + '/' + id,
            data: library  
        });
        return request;
    },

    delete: function (id) {  
        var request = $http({  
            method: "delete",  
            url: apiUrl + '/' + id  
        });  
        return request;  
    } 
  };

  return libraryService;
});
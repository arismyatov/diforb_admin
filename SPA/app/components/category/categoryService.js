app.factory('CategoryService', function($http, Common) {
  var apiUrl = Common.baseApiUrl + '/api/category'; 
  var promise;

  var libraryService = {
    getAll: function() {
      return $http.get(apiUrl); 
    },

    getLibrary: function(id) {
      return $http.get(apiUrl + '/' + id); 
    },

    post: function(category) {
        var request = $http({  
            method: "post",  
            url: apiUrl,  
            data: category  
        });  
        return request; 
    },

    put: function(id, category) {
        var request = $http({  
            method: "put",  
            url: apiUrl + '/' + id,
            data: category  
        });
        return request;
    },

    delete: function (id) {  
        var request = $http({  
            method: "delete",  
            url: apiUrl + '/' + id  
        });  
        return request;  
    } 
  };

  return libraryService;
});
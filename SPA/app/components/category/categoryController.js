app.controller("categoryController", function ($scope, $http, Common, LibraryService, CategoryService) {

        $scope.message = 'Categories';
        $scope.loading = true;
        $scope.addMode = false;
        $scope.newCategory = {};

        loadAllLibraries();

        function loadAllLibraries()
        {  
            var promiseGetLibs = LibraryService.getAll();
            promiseGetLibs.then(function (pl) { 
                $scope.libraries = pl.data;
                loadAllCategories();
            },
            function (errorPl) {
              $scope.error =  errorPl;
            });
        }        

        function loadAllCategories()  
        {  
            var promiseGetCategories = CategoryService.getAll();             
            promiseGetCategories.then(function (pl) { $scope.categories = pl.data },
                  function (errorPl) {  
                      $scope.error =  errorPl;  
                  });  
        }    

        $scope.toggleEdit = function () {
            this.categories.editMode = !this.categories.editMode;
        };

        $scope.toggleAdd = function () {
            $scope.addMode = !$scope.addMode;
        };

        $scope.save = function () {  
            this.newCategory.LibraryId = $scope.newCategory.Library.Id;
            this.newCategory.Library = null;
            var promisePost = CategoryService.post(this.newCategory);

            promisePost.then(function (pl) {  
                alert("New Library is saved Successfully.");
            },
            function (errorPl) {
              $scope.error = 'failure saving Student', errorPl;
            });
        };

        $scope.deleteCategory = function () {
            $scope.loading = true;
            var Id = this.library.Id;

            var promiseDelete = CategoryService.delete(Id);

            promisePost.then(function (pl) {  
                alert("The category is deleted Successfully.");  
            },  
            function (errorPl) {  
              $scope.error = 'failure dalete category', errorPl;  
            });  
        };
    }
);
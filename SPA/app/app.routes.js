var app = angular.module('diforbApp', ['ngRoute']);





// configure the routes
app.config(function($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : '../app/components/home/homeView.html',
            controller  : 'mainController'
        })

        // route for the library page
        .when('/library', {
            templateUrl : '../app/components/library/libraryView.html',
            controller  : 'libraryController'
        })

        // route for the category page
        .when('/library/edit', {
            // templateUrl : '../app/components/category/categoryView.html',
            templateUrl : '../app/components/library/libraryEditView.html',
            controller  : 'libraryController'
        })        

        // route for the category page
        .when('/category', {
            // templateUrl : '../app/components/category/categoryView.html',
            templateUrl : '../app/components/category/categoryView.html',
            controller  : 'categoryController'
        })
        // route for the category page
        .when('/category/edit', {
            // templateUrl : '../app/components/category/categoryView.html',
            templateUrl : '../app/components/category/categoryEditView.html',
            controller  : 'categoryController'
        })
        // route for the soundGroup page
        .when('/soundgroup', {
            // templateUrl : '../app/components/category/categoryView.html',
            templateUrl : '../app/components/soundgroup/soundgroupView.html',
            controller  : 'soundGroupController'
        })
        // route for the soundGroup page
        .when('/soundgroup/edit', {
            // templateUrl : '../app/components/category/categoryView.html',
            templateUrl : '../app/components/soundgroup/soundgroupEditView.html',
            controller  : 'soundGroupController'
        })
        // route for the soundGroup page
        .when('/sound', {
            // templateUrl : '../app/components/category/categoryView.html',
            templateUrl : '../app/components/sound/soundView.html',
            controller  : 'soundController'
        })
        // route for the soundGroup page
        .when('/sound/edit', {
            // templateUrl : '../app/components/category/categoryView.html',
            templateUrl : '../app/components/sound/soundEditView.html',
            controller  : 'soundController'
        });
});

// create the controller and inject Angular's $scope
app.controller('mainController', function($scope) {
    // create a message to display in our view
    $scope.message = 'Home';
});

app.controller('libraryController', function($scope, Common) {
    $scope.message = 'Libraries';
    $scope.baseUrl = Common.baseApiUrl;
    $scope.SubmitForm = function()
    {
    }
});

app.controller('categoryController', function($scope) {
    $scope.message = 'Categories';
});

app.controller('soundGroupController', function($scope) {
    $scope.message = 'Sound Groups';
});

app.controller('soundController', function($scope) {
    $scope.message = 'Sound';
});
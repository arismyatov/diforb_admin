var diforbApp = angular.module('diforbApp', ['ngRoute']);

// configure the routes
diforbApp.config(function($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'app/components/home/libraryView.html',
            controller  : 'mainController'
        })

        // route for the library page
        .when('/library', {
            templateUrl : 'app/components/library/libraryView.html',
            controller  : 'libraryController'
        })

        // route for the category page
        .when('/category', {
            templateUrl : 'app/components/category/categoryView.html',
            controller  : 'categoryController'
        });
});

// create the controller and inject Angular's $scope
diforbApp.controller('mainController', function($scope) {
    // create a message to display in our view
    $scope.message = 'Home';
});

diforbApp.controller('libraryController', function($scope) {
    $scope.message = 'Categories';
});

diforbApp.controller('categoryController', function($scope) {
    $scope.message = 'libaries';
});
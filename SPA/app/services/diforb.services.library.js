(function () {
	angular.module('diforb.services')
		.factory('Library', Libraries);

	angular.module('diforb.services')
		.service('popupService', popupService);	

	Libraries.$inject = ['$resource', 'DiforbConstans'];		

	function Libraries($resource, DiforbConstans)
	{
		var url = DiforbConstans.baseApiUrl + "/api/library/:id";
	    return $resource(url, { id:'@id' }, {
	        update: {
	            method: 'PUT'
	        }
	    });
	}

	function popupService($window) {
	    this.showPopup=function(message) {
	        return $window.confirm(message);
	    }
    }
  
})();
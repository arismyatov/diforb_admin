(function () {
	angular.module('diforb.services')
		.factory('Category', Categories);

	angular.module('diforb.services')
		.service('popupService', popupService);

	Categories.$inject = ['$resource', 'DiforbConstans'];

	function Categories($resource, DiforbConstans)
	{
	    return $resource(DiforbConstans.baseApiUrl + '/api/category/:id', { id:'@id' }, {
	        update: {
	            method: 'PUT'
	        }
	    });
	}

	function popupService($window) {
	    this.showPopup=function(message) {
	        return $window.confirm(message);
	    }
    }
  
})();
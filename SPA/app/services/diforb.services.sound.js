(function () {
	angular.module('diforb.services')
		.factory('Sound', Sounds);

	angular.module('diforb.services')
		.service('popupService', popupService);

	Sounds.$inject = ['$resource', 'DiforbConstans'];

	function Sounds($resource, DiforbConstans)
	{
	    return $resource(DiforbConstans.baseApiUrl + '/api/sound/:id', { id:'@id' }, {
	        update: {
	            method: 'PUT'
	        }
	    });
	}

	function popupService($window) {
	    this.showPopup=function(message) {
	        return $window.confirm(message);
	    }
    }
  
})();
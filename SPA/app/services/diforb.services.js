(function () {
	angular.module('diforb.services', []);

	angular.module('diforb.services')
	    .constant('DiforbConstans', new diforbConstans());

	function diforbConstans() {
		this.baseApiUrl = "http://diforb.azurewebsites.net";
		//this.baseApiUrl = "http://localhost:50881";
		this.availableOrderOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9];
		this.availableSidesOptions = [
			{'id': 0, 'name': 'Left'},
			{'id': 1, 'name': 'Right'}
		];
	} 

})();
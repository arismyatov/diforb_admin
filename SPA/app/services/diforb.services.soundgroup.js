(function () {
	angular.module('diforb.services')
		.factory('SoundGroup', SoundGroups);

	angular.module('diforb.services')
		.service('popupService', popupService);

	SoundGroups.$inject = ['$resource', 'DiforbConstans'];

	function SoundGroups($resource, DiforbConstans)
	{
	    return $resource(DiforbConstans.baseApiUrl + '/api/soundgroup/:id', { id:'@id' }, {
	        update: {
	            method: 'PUT'
	        }
	    });
	}

	function popupService($window) {
	    this.showPopup=function(message) {
	        return $window.confirm(message);
	    }
    }
  
})();
(function () {
	angular.module('diforb.services')
		.factory('SoundFile', SoundFile);

	SoundFile.$inject = ['$resource', 'DiforbConstans'];

	function SoundFile($resource, DiforbConstans)
	{
	    return $resource(DiforbConstans.baseApiUrl + '/api/file/:id', { id:'@id' }, {
	        update: {
	            method: 'PUT'
	        },
	        save: { method: 'POST',
	        		headers: { 'Content-Type': undefined },
	        	    transformRequest: transformRequest
	    	}
	    });
	}

	        // upload: { method: 'POST', 
	        // 		url: DiforbConstans.baseApiUrl + '/api/file/upload',
	        // 	    transformRequest: angular.ident,
	        // 	    headers: { 'Content-Type': undefined } 
	        // 	}        	



	//	        		params: {id: '@id'},

	function transformRequest(data) {
        var formData = new FormData();
        formData.append("model", angular.toJson(data));
        formData.append("file0", data.file);
        return formData;

	    // if (data === undefined)
	    //   return data;

	    // var fd = new FormData();
	    // angular.forEach(data, function(value, key) {
	    //   if (value instanceof FileList) {
	    //     if (value.length == 1) {
	    //       fd.append(key, value[0]);
	    //     } else {
	    //       angular.forEach(value, function(file, index) {
	    //         fd.append(key + '_' + index, file);
	    //       });
	    //     }
	    //   } else {
	    //     fd.append(key, value);
	    //   }
	    // });

	    // return fd;
  }
  
})();
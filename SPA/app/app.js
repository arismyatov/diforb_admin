(function () {
	var app = angular.module('diforbApp', [
									'ui.router',
									'ngResource',
									'LocalStorageModule',
									'diforb.services',
									'diforb.controllers',
									'checklist-model',
									'flow',
									'ngFileUpload'
								 ]);	
	angular.module('diforbApp').config(diforbConfig);

	// angular.module('diforbApp', ['flow'])
	// 	.config(['flowFactoryProvider', function (flowFactoryProvider) {
	// 	    flowFactoryProvider.defaults = {
	// 	        target: '/upload',
	// 	        permanentErrors:[404, 500, 501]
	// 	    };
	// 	    // You can also set default events:
	// 	    // Can be used with different implementations of Flow.js
	// 	    // flowFactoryProvider.factory = fustyFlowFactory;
	// }]);


	app.run(['AuthService', '$rootScope', '$templateCache', function (authService, $rootScope, $templateCache) {
	   $rootScope.$on('$viewContentLoaded', function() {
	      $templateCache.removeAll();
	   });

    	authService.fillAuthData();
	}]);


	function diforbConfig($stateProvider, $httpProvider, $urlRouterProvider)
	{

		// $httpProvider.defaults.transformRequest = function(data) {
		// 	if (data === undefined)
		// 		return data;

		// 	var fd = new FormData();
		// 	angular.forEach(data, function(value, key) {
		// 	  if (value instanceof FileList) {
		// 	    if (value.length == 1) {
		// 	      fd.append(key, value[0]);
		// 	    } else {
		// 	      angular.forEach(value, function(file, index) {
		// 	        fd.append(key + '_' + index, file);
		// 	      });
		// 	    }
		// 	  } else {
		// 	    fd.append(key, value);
		// 	  }
		// 	});

		// 	return fd;
		// }

		// $httpProvider.defaults.headers.post['Content-Type'] = undefined;

		$httpProvider.interceptors.push('authInterceptorService');




		// Libraries
	    $stateProvider.state('home', {
	        url:'/home',
	        templateUrl:'../app/partials/library/libraries.html',
	        controller:'LibraryListController'
	    });	

		// Libraries
	    $stateProvider.state('libraries', {
	        url:'/library',
	        templateUrl:'../app/partials/library/libraries.html',
	        controller:'LibraryListController'
	    });

	    $stateProvider.state('addLibrary', {
	        url:'/library/add',
	        templateUrl:'../app/partials/library/library-add.html',
	        controller:'LibraryCreateController'
	    });

	    $stateProvider.state('editLibrary', {
	        url:'/library/:id/edit',
	        templateUrl:'../app/partials/library/library-edit.html',
	        controller:'LibraryEditController'
	    });

		// SoundGroup
	    $stateProvider.state('soundgroups', {
	        url:'/soundgroup',
	        templateUrl:'../app/partials/soundgroup/soundgroups.html',
	        controller:'SoundGroupListController'
	    });

	    $stateProvider.state('addSoundGroup', {
	        url:'/soundgroup/add',
	        templateUrl:'../app/partials/soundgroup/soundgroup-add.html',
	        controller:'SoundGroupCreateController'
	    });

	    $stateProvider.state('editSoundGroup', {
	        url:'/soundgroup/:id/edit',
	        templateUrl:'../app/partials/soundgroup/soundgroup-edit.html',
	        controller:'SoundGroupEditController'
	    });

	    // Categories
	    $stateProvider.state('categories', {
	        url:'/category',
	        templateUrl:'../app/partials/category/categories.html',
	        controller:'CategoryListController'
	    });

	    $stateProvider.state('addCategory', {
	        url:'/category/add',
	        templateUrl:'../app/partials/category/category-add.html',
	        controller:'CategoryCreateController'
	    });	    

	    $stateProvider.state('editCategory', {
	        url:'/category/:id/edit',
	        templateUrl:'../app/partials/category/category-edit.html',
	        controller:'CategoryEditController'
	    });

	    // Sounds
	    $stateProvider.state('sounds', {
	        url:'/sound',
	        templateUrl:'../app/partials/sound/sounds.html',
	        controller:'SoundListController'
	    });

	    $stateProvider.state('addSound', {
	        url:'/sound/add',
	        templateUrl:'../app/partials/sound/sound-add.html',
	        controller:'SoundCreateController'
	    });	    

	    $stateProvider.state('editSound', {
	        url:'/sound/:id/edit',
	        templateUrl:'../app/partials/sound/sound-edit.html',
	        controller:'SoundEditController'
	    });

	    // File

	    $stateProvider.state('files', {
	        url:'/file',
	        templateUrl:'../app/partials/file/files.html',
	        controller:'FileListController'
	    });

	    $stateProvider.state('filesuploadbulk', {
	        url:'/filesuploadbulk',
	        templateUrl:'../app/partials/file/file-upload-bulk.html',
	        controller:'FileBulkUploadController'
	    });

	    $stateProvider.state('addFile', {
	        url:'/file/add',
	        templateUrl:'../app/partials/file/file-add.html',
	        controller:'FileCreateController'
	    });	    

	    $stateProvider.state('editFile', {
	        url:'/file/{id}/edit',
	        templateUrl:'../app/partials/file/file-edit.html',
	        controller:'FileEditController'
	    });

	    // account

	    $stateProvider.state('login', {
	    	url:'/login',
	        controller: "LoginController",
	        templateUrl: "../app/partials/account/login.html"
	    });	    

	    $stateProvider.state('signup', {
	    	url:'/signup',
	        controller: "SignUpController",
	        templateUrl: "../app/partials/account/signup.html"
	    });	    

	    $urlRouterProvider.otherwise('/library');
	}

})();


// (function () {
//     'use strict';

//     angular.module('fs.controllers')
//         .controller('HeadController', HeadController);

//     HeadController.$inject = ['authHelper'];

//     function HeadController(authHelper) {
//         /* jshint validthis: true */
//         var headVm = this;

//         headVm.logout = logout;

//         function logout() {
//             authHelper.logout();
//         }
//     }

// })();
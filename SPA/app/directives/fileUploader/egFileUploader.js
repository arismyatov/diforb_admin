﻿(function () {
    'use strict';

    angular
        .module('diforbApp')
        .directive('egFileUploader', egSoundFileUploader);

    egSoundFileUploader.$inject = ['$parse'];

    function egSoundFileUploader($parse) {

        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            var model = $parse(attrs.egFileUploader);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    }

})();
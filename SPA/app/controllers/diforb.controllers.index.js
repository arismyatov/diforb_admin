(function () {
	angular.module('diforb.controllers')
		.controller('IndexController', IndexController);

	IndexController.$inject = ['$scope', '$state', 'AuthService'];

	function IndexController($scope, $state, authService)	{
	    $scope.logOut = function () {
	        authService.logOut();
	        $state.go('login');
	    }

	    $scope.authentication = authService.authentication;
	};

})();
(function () {
	angular.module('diforb.controllers')
		.controller('SignUpController', SignUpController);

	angular.module('diforb.controllers')
		.controller('LoginController', LoginController);	

	SignUpController.$inject = ['$scope', '$state', '$timeout', 'AuthService'];
	LoginController.$inject = ['$scope', '$state', 'AuthService'];	

	function SignUpController($scope, $state, $timeout, authService)	{
	    $scope.savedSuccessfully = false;
	    $scope.message = "";

	    $scope.registration = {
	        userName: "",
	        password: "",
	        confirmPassword: ""
	    };

	    $scope.signUp = function () {

	        authService.saveRegistration($scope.registration).then(function (response) {

	            $scope.savedSuccessfully = true;
	            $scope.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
	            startTimer();

	        },
	         function (response) {
	             var errors = [];
	             for (var key in response.data.modelState) {
	                 for (var i = 0; i < response.data.modelState[key].length; i++) {
	                     errors.push(response.data.modelState[key][i]);
	                 }
	             }
	             $scope.message = "Failed to register user due to:" + errors.join(' ');
	         });
	    };

	    var startTimer = function () {
	        var timer = $timeout(function () {
	            $timeout.cancel(timer);
	            $state.go('login');
	        }, 2000);
	    }
	};


	function LoginController($scope, $state, authService)
	{
	    $scope.loginData = {
	        userName: "",
	        password: ""
	    };

	    $scope.message = "";

	    $scope.login = function () {
	        authService.login($scope.loginData).then(function (response) {
	            $state.go('libraries');
	        },
	        function (err) {
	            $scope.message = err.error_description;
	        });
	    };   
	};

})();
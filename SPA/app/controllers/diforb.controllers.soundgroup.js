(function () {
	angular.module('diforb.controllers')
		.controller('SoundGroupListController', SoundGroupListController);

	angular.module('diforb.controllers')
		.controller('SoundGroupCreateController', SoundGroupCreateController);	

	angular.module('diforb.controllers')
		.controller('SoundGroupEditController', SoundGroupEditController);	


	SoundGroupListController.$inject   = ['SoundGroup', 'popupService', '$state'];
	SoundGroupCreateController.$inject = ['SoundGroup', 'Library','$scope', '$state', 'DiforbConstans'];
	SoundGroupEditController.$inject = ['SoundGroup', 'Library', '$scope', '$state', '$stateParams', 'DiforbConstans'];

	function SoundGroupListController(SoundGroup, popupService, $state)	{
		var SoundGroupVm = this;
		
		SoundGroupVm.title = "Sound groups";
	    SoundGroupVm.deleteSoundGroup = dleteSoundGroup;

	    getSoundGroups();

	    function dleteSoundGroup(SoundGroup)
	    {
	        if(popupService.showPopup('Really delete this SoundGroup?')) {
	            SoundGroup.$delete(function(){
					getSoundGroups();	            	
	                $state.go('soundgroups');
	            });
	        }
	    }

	    function getSoundGroups()
	    {
	    	SoundGroupVm.soundgroups = SoundGroup.query();
	    }
	}


	function SoundGroupCreateController(SoundGroup, Library, $scope, $state, DiforbConstans)
	{
	    $scope.formTitle = "New SoundGroup";		
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;
	    $scope.availableSidesOptions = DiforbConstans.availableSidesOptions;
	    $scope.libraries = Library.query();
	    $scope.selectedLibrary = {};
	    $scope.selectedSide    = {};
	    $scope.SoundGroup   = new SoundGroup();

	    $scope.addSoundGroup = addSoundGroup;
	    $scope.cancel     = cancel;

	    function addSoundGroup() {
	        $scope.SoundGroup.$save( function() {
	            $state.go('soundgroups');
	        });
	    }

	    function cancel() {
	    	$state.go('soundgroups');
	    }    
	}

	function SoundGroupEditController(SoundGroup, Library, $scope, $state, $stateParams, DiforbConstans)
    {
	    $scope.formTitle = "Edit SoundGroup";
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;
	    $scope.availableSidesOptions = DiforbConstans.availableSidesOptions;
	    $scope.libraries = Library.query();

	    $scope.loadSoundGroup   = loadSoundGroup;
	    $scope.updateSoundGroup = updateSoundGroup;
	    $scope.cancel        = cancel;

	    $scope.loadSoundGroup();	    

	    function loadSoundGroup(){
	        $scope.SoundGroup = SoundGroup.get({id:$stateParams.id});
	    };

	    function updateSoundGroup(){
	        $scope.SoundGroup.$update(function(){
	            $state.go('soundgroups');
	        });
	    };

	    function cancel() {
	    	$state.go('soundgroups');
	    }  	    
	}

})();
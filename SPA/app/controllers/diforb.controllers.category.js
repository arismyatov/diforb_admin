(function () {
	angular.module('diforb.controllers')
		.controller('CategoryListController', CategoryListController);

	angular.module('diforb.controllers')
		.controller('CategoryCreateController', CategoryCreateController);	

	angular.module('diforb.controllers')
		.controller('CategoryEditController', CategoryEditController);	


	CategoryListController.$inject   = ['Category', 'popupService', '$state'];
	CategoryCreateController.$inject = ['Category', 'Library', 'SoundGroup','$scope', '$state', 'DiforbConstans'];
	CategoryEditController.$inject   = ['Category', 'Library', 'SoundGroup', '$scope', '$state', '$stateParams', 'DiforbConstans'];

	function CategoryListController(Category, popupService, $state)	{
		var CategoryVm = this;
		
		CategoryVm.title = "Category";
	    CategoryVm.deleteCategory = dleteCategory;

	    getCategories();

	    function dleteCategory(Category)
	    {
	        if(popupService.showPopup('Really delete this Category?')) {
	            Category.$delete(function(){
					getCategories();	            	
	                $state.go('Categories');
	            });
	        }
	    }

	    function getCategories()
	    {
	    	CategoryVm.Categories = Category.query();
	    }
	}


	function CategoryCreateController(Category, Library, SoundGroup, $scope, $state, DiforbConstans)
	{
	    $scope.formTitle = "New Category";		
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;
	    $scope.availableSidesOptions = DiforbConstans.availableSidesOptions;
	    $scope.libraries = Library.query();
		$scope.soundGroups = SoundGroup.query();
	    $scope.parentCategories = Category.query();
	    // $scope.selectedLibrary = {};
	    // $scope.selectedSide    = {};
	    $scope.Category = new Category();

	    $scope.addCategory = addCategory;
	    $scope.cancel = cancel;

	    //filters
	    $scope.hasLibrary = hasLibrary;
	    $scope.soundGroupHasLibrary = soundGroupHasLibrary;    
	    $scope.compareSoundGroup = compareSoundGroup;

	    function compareSoundGroup(obj1, obj2) {
	        return obj1.id === obj2.id;
	    };	    

	    function addCategory() {
	        $scope.Category.$save( function() {
	            $state.go('categories');
	        });
	    }

	    function hasLibrary(category){
	    	return category.libraryId == $scope.Category.libraryId;
	    }

	    function soundGroupHasLibrary(soundGroup){
	    	return soundGroup.libraryId == $scope.Category.libraryId;
	    }    

	    function cancel() {
	    	$state.go('categories');
	    }    
	}

	function CategoryEditController(Category, Library, SoundGroup, $scope, $state, $stateParams, DiforbConstans)
    {
	    $scope.formTitle = "Edit Category";    	
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;
	    $scope.availableSidesOptions = DiforbConstans.availableSidesOptions;	    
	    $scope.libraries  = Library.query();
	    $scope.soundGroups = SoundGroup.query();
	    $scope.parentCategories = Category.query();

	    $scope.loadCategory   = loadCategory;
	    $scope.updateCategory = updateCategory;
	    $scope.hasLibrary     = hasLibrary;
	    $scope.cancel         = cancel;
	    $scope.compareSoundGroup = compareSoundGroup;	    

	    $scope.loadCategory();	    

	    function compareSoundGroup(obj1, obj2) {
	        return obj1.id === obj2.id;
	    };	    

	    function loadCategory(){
	        $scope.Category = Category.get({id:$stateParams.id});
	    };

	    function updateCategory(){
	        $scope.Category.$update(function(){
	            $state.go('categories');
	        });
	    };

	    function hasLibrary(category){
	    	return category.libraryId == $scope.Category.libraryId;
	    };

	    function cancel() {
	    	$state.go('categories');
	    }  	    
	}

})();
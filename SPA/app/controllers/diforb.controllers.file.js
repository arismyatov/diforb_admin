(function () {
	angular.module('diforb.controllers')
		.controller('FileListController', FileListController);

	angular.module('diforb.controllers')
		.controller('FileCreateController', FileCreateController);	

	angular.module('diforb.controllers')
		.controller('FileEditController', FileEditController);	

	angular.module('diforb.controllers')
		.controller('FileBulkUploadController', FileBulkUploadController);	

	FileListController.$inject   = ['SoundFile', 'popupService', '$state'];
	FileCreateController.$inject = ['SoundFile', 'Library', 'Category', 'Sound', '$scope', '$state', 'DiforbConstans'];
	FileEditController.$inject = ['SoundFile', 'Library', 'Sound','$scope', '$state', '$stateParams', 'DiforbConstans'];
	FileBulkUploadController.$inject = ['$scope', 'Upload', '$timeout', 'DiforbConstans'];

	function FileListController(SoundFile, popupService, $state, DiforbConstans)	{
		var FileVm = this;
		
		FileVm.title = "Files";
	    FileVm.deleteFile = dleteFile;

	    getFiles();

	    function dleteFile(SoundFile)
	    {
	        if(popupService.showPopup('Really delete this File?')) {
	            SoundFile.$delete(function(){
					getFiles();	            	
	                $state.go('files');
	            });
	        }
	    }

	    function getFiles()
	    {
	    	FileVm.files = SoundFile.query();
	    }
	}

	function FileBulkUploadController($scope, Upload, $timeout)	{

			$scope.$watch('files', function () {
			        $scope.upload($scope.files);
			    });

		    $scope.$watch('file', function () {
		        if ($scope.file != null) {
		            $scope.files = [$scope.file]; 
		        }
		    });

		    $scope.log = '';

		    $scope.upload = function (files) {
		        if (files && files.length) {
		            for (var i = 0; i < files.length; i++) {
		              var file = files[i];
		              if (!file.$error) {

		              	var uploadUrl = DiforbConstans.baseApiUrl + '/api/file/uploadbulk';

		                Upload.upload({
		                    url: uploadUrl,
		                    data: {
		                      username: $scope.username,
		                      file: file  
		                    }
		                }).then(function (resp) {
		                    $timeout(function() {
		                        $scope.log = 'file: ' +
		                        resp.config.data.file.name +
		                        ', Response: ' + JSON.stringify(resp.data) +
		                        '\n' + $scope.log;
		                    });
		                }, null, function (evt) {
		                    var progressPercentage = parseInt(100.0 *
		                    		evt.loaded / evt.total);
		                    $scope.log = 'progress: ' + progressPercentage + 
		                    	'% ' + evt.config.data.file.name + '\n' + 
		                      $scope.log;
		                });
		            }
		        }
		    }
		}

	}


	function FileCreateController(SoundFile, Library, Category, Sound, $scope, $state, DiforbConstans)
	{
	    $scope.formTitle = "New File";		
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;
	    $scope.availableSidesOptions = DiforbConstans.availableSidesOptions;
	    $scope.libraries = Library.query();
		$scope.categories = Category.query();	    
		$scope.sounds = Sound.query();	    		
	    $scope.selectedLibrary = {};
	    $scope.selectedSide    = {};
	    $scope.File   = new SoundFile();

	    $scope.addFile = addFile;
	    $scope.cancel     = cancel;
	    $scope.uploadFiles  = uploadFiles;

	    //filters
	    $scope.hasLibrary  = hasLibrary;
		$scope.hasCategory = hasCategory;

	    function addFile() {
	        $scope.File.$save( function(data) {
	        	console.log("File is uploaded");
	        	$state.go('files');
	        });
	    }

	    function uploadFiles(pathWithName) {
 			var formData = new FormData();
 			formData.append(pathWithName, $scope.attachment.file);
            return soundFile.upload(formData)
                                        .$promise
                                        .then(function (result) {
                                     		$state.go('files');
                                        },
                                        function (result) {
                                        })
                                        ['finally'](
                                        function () {
                                        });
	    }    

	    function hasLibrary(item){
	    	return item.libraryId == $scope.File.libraryId;
	    }	    

	   	function hasLibraryNotParrent(item){
	    	return item.libraryId == $scope.File.libraryId && item.parentCategoryId == null;
	    }	

	    function hasCategory(item){
	    	return item.categories && 
	    		   item.categories.filter(function(category) { return category.id == $scope.File.categoryId; }).length > 0;

	    	//item.categories.indexOf($scope.File.categoryId) > -1;
	    	// return item.categories. == $scope.File.categoryId;
	    }	    


	    function cancel() {
	    	$state.go('files');
	    }    
	}

	function FileEditController(SoundFile, Library, $scope, $state, $stateParams, DiforbConstans)
    {

	    $scope.formTitle = "Edit File";
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;
	    $scope.availableSidesOptions = DiforbConstans.availableSidesOptions;
	    $scope.libraries = Library.query();

	    $scope.loadFile   = loadFile;
	    $scope.updateFile = updateFile;
	    $scope.cancel        = cancel;

	    
	    setTimeout($scope.loadFile(), 3000);	    

	    function loadFile(){
	        $scope.File = SoundFile.get({ id:$stateParams.params.id });
	    };

	    function updateFile(){
	        $scope.File.$update(function(){
	            $state.go('soundgroups');
	        });
	    };

	    function cancel() {
	    	$state.go('soundgroups');
	    }  	    
	}

})();
(function () {
	angular.module('diforb.controllers')
		.controller('SoundListController', SoundListController);

	angular.module('diforb.controllers')
		.controller('SoundCreateController', SoundCreateController);	

	angular.module('diforb.controllers')
		.controller('SoundEditController', SoundEditController);	


	SoundListController.$inject   = ['Sound', 'popupService', '$state'];
	SoundCreateController.$inject = ['Sound', 'Library', 'Category', '$scope', '$state', 'DiforbConstans'];
	SoundEditController.$inject   = ['Sound', 'Library', 'Category','$scope', '$state', '$stateParams', 'DiforbConstans'];

	function SoundListController(Sound, popupService, $state)	{
		var SoundVm = this;
		
		SoundVm.title = "Sound";
	    SoundVm.deleteSound = dleteSound;

	    getSounds();

	    function dleteSound(Sound)
	    {
	        if(popupService.showPopup('Really delete this Sound?')) {
	            Sound.$delete(function(){
					getSounds();	            	
	                $state.go('Sounds');
	            });
	        }
	    }

	    function getSounds()
	    {
	    	SoundVm.Sounds = Sound.query();
	    }
	}


	function SoundCreateController(Sound, Library, Category, $scope, $state, DiforbConstans)
	{
	    $scope.formTitle = "New Sound";		
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;
	    $scope.libraries = Library.query();
		$scope.categories = Category.query();	    
	    $scope.selectedLibrary = {};
	    $scope.selectedSide    = {};
	    $scope.selectedRoles = [];
	    $scope.errorMessage = "";   
	    $scope.compareCategories = compareCategories;    

	    $scope.Sound   = new Sound();
	    // $scope.Sound.categories  = [];

	    $scope.addSound = addSound;
	    $scope.cancel     = cancel;

	    //filters
	    $scope.hasLibrary = hasLibrary;

	    function addSound() {
	        $scope.Sound.$save( function() {
	            $state.go('sounds');
	        },
	        function (error) {
	        	$scope.errorMessage = error.data.message;
            });
	    }

	    function cancel() {
	    	$state.go('sounds');
	    }    

	    function hasLibrary(item){
	    	return item.libraryId == $scope.Sound.libraryId;
	    }    

	    function compareCategories(obj1, obj2) {
	        return obj1.id === obj2.id;
	    };

	}

	function SoundEditController(Sound, Library, Category, $scope, $state, $stateParams, DiforbConstans)
    {
	    $scope.formTitle = "Edit Sound";
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;
	    $scope.libraries  = Library.query();
	    $scope.categories = Category.query();

	    $scope.loadSound   = loadSound;
	    $scope.updateSound = updateSound;
	    $scope.hasLibrary     = hasLibrary;
	    $scope.cancel         = cancel;    
	    $scope.compareCategories = compareCategories;    
		$scope.errorMessage = "";	    

	    $scope.loadSound();	    

	    function compareCategories(obj1, obj2) {
	        return obj1.id === obj2.id;
	    };

	    function loadSound(){
	        $scope.Sound = Sound.get({id:$stateParams.id});
	    };

	    function updateSound(){
	        $scope.Sound.$update(function(){
	            $state.go('sounds');
	        });
	    };

	    function hasLibrary(category){
	    	return category.libraryId == $scope.Sound.libraryId;
	    };

	    function cancel() {
	    	$state.go('sounds');
	    }  	    
	}

})();
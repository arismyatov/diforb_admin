(function () {
	angular.module('diforb.controllers')
		.controller('LibraryListController', LibraryListController);

	angular.module('diforb.controllers')
		.controller('LibraryCreateController', LibraryCreateController);	

	angular.module('diforb.controllers')
		.controller('LibraryEditController', LibraryEditController);	


	LibraryListController.$inject   = ['Library', 'popupService', '$state'];
	LibraryCreateController.$inject = ['Library', '$scope', '$state', 'DiforbConstans'];
	LibraryEditController.$inject = ['Library', '$scope', '$state', '$stateParams', 'DiforbConstans'];

	function LibraryListController(Library, popupService, $state)	{
		var libraryVm = this;
		
		libraryVm.message = "Libraries";
	    libraryVm.deleteLibrary = dleteLibrary;

	    getLibraries();

	    function dleteLibrary(library)
	    {
	        if(popupService.showPopup('Really delete this library?')) {
	            library.$delete(function(){
					getLibraries();	            	
	                $state.go('libraries');
	            });
	        }
	    }

	    function getLibraries()
	    {
	    	libraryVm.libraries = Library.query();
	    }
	}


	function LibraryCreateController(Library, $scope, $state, DiforbConstans)
	{
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;		
	    $scope.library   = new Library();
	    $scope.formTitle = "New Library";

	    $scope.addLibrary = addLibrary;
	    $scope.cancel     = cancel;

	    function addLibrary() {
	        $scope.library.$save( function() {
	            $state.go('libraries');
	        });
	    }

	    function cancel() {
	    	$state.go('libraries');
	    }    
	}

	function LibraryEditController(Library, $scope, $state, $stateParams, DiforbConstans)
    {
	    $scope.formTitle = "Edit Library";
	    $scope.availableOrderOptions = DiforbConstans.availableOrderOptions;

	    $scope.loadLibrary   = loadLibrary;
	    $scope.updateLibrary = updateLibrary;
	    $scope.cancel        = cancel;

	    $scope.loadLibrary();

	    function loadLibrary(){
	        $scope.library = Library.get({id:$stateParams.id});
	    };

	    function updateLibrary(){
	        $scope.library.$update(function(){
	            $state.go('libraries');
	        });
	    };

	    function cancel() {
	    	$state.go('libraries');
	    }  	    
	}

})();
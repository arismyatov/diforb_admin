﻿using System.ComponentModel.DataAnnotations;
using Diforb.DAL.Classes.Abstract;
using Diforb.DAL.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diforb.DAL.Classes
{
    public class Category2Category
    {
        [Key]
        public Guid Id { get; set; }

        public Guid ParentCategoryId { get; set; }

        public Guid ChildCategoryId { get; set; }
    }
}
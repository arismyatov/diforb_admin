﻿using System.ComponentModel.DataAnnotations;
using Diforb.DAL.Classes.Abstract;
using Diforb.DAL.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diforb.DAL.Classes
{
    public class Subscriber
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
    }
}

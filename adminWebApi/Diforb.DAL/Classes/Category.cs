﻿using System.ComponentModel.DataAnnotations;
using Diforb.DAL.Classes.Abstract;
using Diforb.DAL.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diforb.DAL.Classes
{
    public class Category : DiforbEntityIcon
    {
        [Required]
        public Guid LibraryId { get; set; }

        public virtual Library Library { get; set; }

        public Guid? ParentCategoryId { get; set; }

        public virtual Category ParentCategory { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

        public virtual ICollection<Sound> Sounds { get; set; }

        public virtual ICollection<SoundGroup> SoundGroups { get; set; }

        public virtual ICollection<File> Files { get; set; }

    }
}

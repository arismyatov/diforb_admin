﻿using System.ComponentModel.DataAnnotations;
using Diforb.DAL.Classes.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diforb.DAL.Classes
{
    public class Sound : DiforbEntityIcon
    {
        public virtual ICollection<Category> Categories { get; set; }

        public virtual ICollection<File> Files { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}
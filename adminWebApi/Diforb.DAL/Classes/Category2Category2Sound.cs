﻿using System.ComponentModel.DataAnnotations;
using Diforb.DAL.Classes.Abstract;
using Diforb.DAL.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diforb.DAL.Classes
{
    public class Category2Category2Sound
    {
        [Key]
        public Guid Id { get; set; }

        public Category2Category Category2CategoryId { get; set; }

        public Guid SoundId { get; set; }
    }
}
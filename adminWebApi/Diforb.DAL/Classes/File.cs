﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Diforb.DAL.Classes
{
    public class File
    {
        [Required]
        public Guid Id { get; set; }

        public string PathWithName { get; set; }

        [Required]
        public Guid SoundId { get; set; }

        public virtual Sound Sound { get; set; }

        [Required]
        public Guid CategoryId { get; set; }

        public virtual Category Category { get; set; }
   }
}
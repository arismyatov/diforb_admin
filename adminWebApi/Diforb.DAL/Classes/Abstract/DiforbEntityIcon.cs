﻿
namespace Diforb.DAL.Classes.Abstract
{
    public abstract class DiforbEntityIcon : DiforbEntity
    {
        public string Icon { get; set; }
    }
}

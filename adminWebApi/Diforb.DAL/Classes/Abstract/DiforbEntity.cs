﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Diforb.DAL.Classes.Abstract
{
    public abstract class DiforbEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(100,
        ErrorMessage = "Name must be less than 100 characters."),
        MinLength(2,
        ErrorMessage = "Name must be more than 2 characters.")]
        public string Name { get; set; }

        [Required]
        [StringLength(100,
        ErrorMessage = "Name must be less than 100 characters."),
        MinLength(2,
        ErrorMessage = "Name must be more than 2 characters.")]
        public string Title { get; set; }

        [StringLength(100,
        ErrorMessage = "Name must be less than 150 characters.")]
        public string Description { get; set; }

        public int Order { get; set; }
    }
}

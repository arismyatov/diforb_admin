﻿using Diforb.DAL.Classes.Abstract;
using Diforb.DAL.Classes.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diforb.DAL.Classes
{
    public class SoundGroup : DiforbEntityIcon
    {
        [Required]
        [EnumDataType(typeof(Side))]
        public Side Side { get; set; }

        [Required]
        public Guid LibraryId { get; set; }

        public virtual Library Library { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
    }
}

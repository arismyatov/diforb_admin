﻿using System.ComponentModel.DataAnnotations.Schema;
using Diforb.DAL.Classes.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diforb.DAL.Classes
{
    public class Library : DiforbEntityIcon
    {
        public string AudioSrc { get; set; }

        public string ImageSrc { get; set; }

        public string Author { get; set; }

        public Guid CareatorId { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

        public virtual ICollection<SoundGroup> SoundGroups { get; set; }
    }
}
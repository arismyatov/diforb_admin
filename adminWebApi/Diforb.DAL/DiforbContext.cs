﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diforb.DAL.Classes;

namespace Diforb.DAL
{
    public class DiforbContext : DbContext
    {
        public DiforbContext()
            : base("DiforbDalConnection")
        {
        }

        public DbSet<Library> Libraries { get; set; }
        public DbSet<SoundGroup> SoundGroups { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Sound> Sounds { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException("modelBuilder");
            }

            OnLibraryCreating(modelBuilder);
            OnCategoryCreating(modelBuilder);
            OnSoundGroupCreating(modelBuilder);
            OnSoundCreating(modelBuilder);
            OnTagCreating(modelBuilder);
            OnFileCreating(modelBuilder);
        }

        private void OnLibraryCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Library>()
                .HasMany<Category>(s => s.Categories)
                .WithRequired(s => s.Library)
                .HasForeignKey(s => s.LibraryId);

            modelBuilder
                .Entity<Library>()
                .HasMany<SoundGroup>(s => s.SoundGroups)
                .WithRequired(s => s.Library)
                .HasForeignKey(s => s.LibraryId);
        }


        private void OnCategoryCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Category>()
                .HasRequired<Library>(c => c.Library)
                .WithMany(p => p.Categories)
                .HasForeignKey(c => c.LibraryId);
            modelBuilder
                .Entity<Category>()
                .HasOptional<Category>(c => c.ParentCategory)
                .WithMany(c => c.Categories)
                .HasForeignKey(k => k.ParentCategoryId)
                .WillCascadeOnDelete(false);
        }


        private void OnSoundGroupCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<SoundGroup>()
                .HasRequired<Library>(p => p.Library)
                .WithMany(p => p.SoundGroups)
                .HasForeignKey(s => s.LibraryId)
                .WillCascadeOnDelete(false);
            modelBuilder
                .Entity<SoundGroup>()
                .HasMany<Category>(s => s.Categories)
                .WithMany(s => s.SoundGroups);
        }


        private void OnSoundCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Sound>()
                .HasMany<Category>(p => p.Categories)
                .WithMany(p => p.Sounds);
            modelBuilder
                .Entity<Sound>()
                .HasMany<File>(s => s.Files)
                .WithRequired(s => s.Sound)
                .HasForeignKey(s => s.SoundId);
            modelBuilder
                .Entity<Sound>()
                .HasMany<Tag>(s => s.Tags)
                .WithMany(s => s.Sounds);
        }

        private void OnTagCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Tag>()
                .HasMany<Sound>(p => p.Sounds)
                .WithMany(p => p.Tags);
        }

        private void OnFileCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<File>()
                .HasRequired<Sound>(f => f.Sound)
                .WithMany(f => f.Files)
                .HasForeignKey(f => f.SoundId);
            modelBuilder
                .Entity<File>()
                .HasRequired<Category>(f => f.Category)
                .WithMany(f => f.Files)
                .HasForeignKey(f => f.CategoryId);
        }
    }
}

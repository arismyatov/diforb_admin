namespace Diforb.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class files : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "CategoryId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Files", "PathWithName", c => c.String());
            CreateIndex("dbo.Files", "CategoryId");
            AddForeignKey("dbo.Files", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Files", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Files", new[] { "CategoryId" });
            AlterColumn("dbo.Files", "PathWithName", c => c.String(nullable: false));
            DropColumn("dbo.Files", "CategoryId");
        }
    }
}

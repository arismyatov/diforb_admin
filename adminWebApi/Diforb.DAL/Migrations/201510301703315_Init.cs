namespace Diforb.DAL.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        LibraryId = c.Guid(nullable: false),
                        ParentCategoryId = c.Guid(),
                        Icon = c.String(),
                        Name = c.String(nullable: false, maxLength: 100),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 100),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Libraries", t => t.LibraryId, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.ParentCategoryId)
                .Index(t => t.LibraryId)
                .Index(t => t.ParentCategoryId);
            
            CreateTable(
                "dbo.Libraries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Icon = c.String(),
                        Name = c.String(nullable: false, maxLength: 100),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 100),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SoundGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Side = c.Int(nullable: false),
                        LibraryId = c.Guid(nullable: false),
                        Icon = c.String(),
                        Name = c.String(nullable: false, maxLength: 100),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 100),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Libraries", t => t.LibraryId)
                .Index(t => t.LibraryId);
            
            CreateTable(
                "dbo.Sounds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Icon = c.String(),
                        Name = c.String(nullable: false, maxLength: 100),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 100),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PathWithName = c.String(nullable: false),
                        SoundId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sounds", t => t.SoundId, cascadeDelete: true)
                .Index(t => t.SoundId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SoundGroupCategories",
                c => new
                    {
                        SoundGroup_Id = c.Guid(nullable: false),
                        Category_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.SoundGroup_Id, t.Category_Id })
                .ForeignKey("dbo.SoundGroups", t => t.SoundGroup_Id, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.SoundGroup_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.SoundCategories",
                c => new
                    {
                        Sound_Id = c.Guid(nullable: false),
                        Category_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sound_Id, t.Category_Id })
                .ForeignKey("dbo.Sounds", t => t.Sound_Id, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.Sound_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.SoundTags",
                c => new
                    {
                        Sound_Id = c.Guid(nullable: false),
                        Tag_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sound_Id, t.Tag_Id })
                .ForeignKey("dbo.Sounds", t => t.Sound_Id, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .Index(t => t.Sound_Id)
                .Index(t => t.Tag_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SoundTags", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.SoundTags", "Sound_Id", "dbo.Sounds");
            DropForeignKey("dbo.Files", "SoundId", "dbo.Sounds");
            DropForeignKey("dbo.SoundCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.SoundCategories", "Sound_Id", "dbo.Sounds");
            DropForeignKey("dbo.Categories", "ParentCategoryId", "dbo.Categories");
            DropForeignKey("dbo.Categories", "LibraryId", "dbo.Libraries");
            DropForeignKey("dbo.SoundGroups", "LibraryId", "dbo.Libraries");
            DropForeignKey("dbo.SoundGroupCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.SoundGroupCategories", "SoundGroup_Id", "dbo.SoundGroups");
            DropIndex("dbo.SoundTags", new[] { "Tag_Id" });
            DropIndex("dbo.SoundTags", new[] { "Sound_Id" });
            DropIndex("dbo.SoundCategories", new[] { "Category_Id" });
            DropIndex("dbo.SoundCategories", new[] { "Sound_Id" });
            DropIndex("dbo.SoundGroupCategories", new[] { "Category_Id" });
            DropIndex("dbo.SoundGroupCategories", new[] { "SoundGroup_Id" });
            DropIndex("dbo.Files", new[] { "SoundId" });
            DropIndex("dbo.SoundGroups", new[] { "LibraryId" });
            DropIndex("dbo.Categories", new[] { "ParentCategoryId" });
            DropIndex("dbo.Categories", new[] { "LibraryId" });
            DropTable("dbo.SoundTags");
            DropTable("dbo.SoundCategories");
            DropTable("dbo.SoundGroupCategories");
            DropTable("dbo.Tags");
            DropTable("dbo.Files");
            DropTable("dbo.Sounds");
            DropTable("dbo.SoundGroups");
            DropTable("dbo.Libraries");
            DropTable("dbo.Categories");
        }
    }
}

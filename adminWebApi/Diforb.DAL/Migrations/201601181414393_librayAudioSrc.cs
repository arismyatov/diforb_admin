namespace Diforb.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class librayAudioSrc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Libraries", "AudioSrc", c => c.String());
            AddColumn("dbo.Libraries", "ImageSrc", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Libraries", "ImageSrc");
            DropColumn("dbo.Libraries", "AudioSrc");
        }
    }
}

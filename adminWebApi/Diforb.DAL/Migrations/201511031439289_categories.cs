namespace Diforb.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class categories : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Categories", "SoundGroupId", "dbo.SoundGroups");
            DropForeignKey("dbo.Sounds", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.SoundGroups", "LibraryId", "dbo.Libraries");
            DropIndex("dbo.Categories", new[] { "SoundGroupId" });
            DropIndex("dbo.Sounds", new[] { "CategoryId" });
            RenameColumn(table: "dbo.Categories", name: "ParenCategory_Id", newName: "ParentCategoryId");
            RenameIndex(table: "dbo.Categories", name: "IX_ParenCategory_Id", newName: "IX_ParentCategoryId");
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SoundGroupCategories",
                c => new
                    {
                        SoundGroup_Id = c.Guid(nullable: false),
                        Category_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.SoundGroup_Id, t.Category_Id })
                .ForeignKey("dbo.SoundGroups", t => t.SoundGroup_Id, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.SoundGroup_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.SoundCategories",
                c => new
                    {
                        Sound_Id = c.Guid(nullable: false),
                        Category_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sound_Id, t.Category_Id })
                .ForeignKey("dbo.Sounds", t => t.Sound_Id, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.Sound_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.SoundTags",
                c => new
                    {
                        Sound_Id = c.Guid(nullable: false),
                        Tag_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sound_Id, t.Tag_Id })
                .ForeignKey("dbo.Sounds", t => t.Sound_Id, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .Index(t => t.Sound_Id)
                .Index(t => t.Tag_Id);
            
            AddColumn("dbo.Categories", "LibraryId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Categories", "LibraryId");
            AddForeignKey("dbo.Categories", "LibraryId", "dbo.Libraries", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SoundGroups", "LibraryId", "dbo.Libraries", "Id");
            DropColumn("dbo.Categories", "SoundGroupId");
            DropColumn("dbo.Categories", "CategoryId");
            DropColumn("dbo.Sounds", "CategoryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sounds", "CategoryId", c => c.Guid(nullable: false));
            AddColumn("dbo.Categories", "CategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.Categories", "SoundGroupId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.SoundGroups", "LibraryId", "dbo.Libraries");
            DropForeignKey("dbo.SoundTags", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.SoundTags", "Sound_Id", "dbo.Sounds");
            DropForeignKey("dbo.SoundCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.SoundCategories", "Sound_Id", "dbo.Sounds");
            DropForeignKey("dbo.Categories", "LibraryId", "dbo.Libraries");
            DropForeignKey("dbo.SoundGroupCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.SoundGroupCategories", "SoundGroup_Id", "dbo.SoundGroups");
            DropIndex("dbo.SoundTags", new[] { "Tag_Id" });
            DropIndex("dbo.SoundTags", new[] { "Sound_Id" });
            DropIndex("dbo.SoundCategories", new[] { "Category_Id" });
            DropIndex("dbo.SoundCategories", new[] { "Sound_Id" });
            DropIndex("dbo.SoundGroupCategories", new[] { "Category_Id" });
            DropIndex("dbo.SoundGroupCategories", new[] { "SoundGroup_Id" });
            DropIndex("dbo.Categories", new[] { "LibraryId" });
            DropColumn("dbo.Categories", "LibraryId");
            DropTable("dbo.SoundTags");
            DropTable("dbo.SoundCategories");
            DropTable("dbo.SoundGroupCategories");
            DropTable("dbo.Tags");
            RenameIndex(table: "dbo.Categories", name: "IX_ParentCategoryId", newName: "IX_ParenCategory_Id");
            RenameColumn(table: "dbo.Categories", name: "ParentCategoryId", newName: "ParenCategory_Id");
            CreateIndex("dbo.Sounds", "CategoryId");
            CreateIndex("dbo.Categories", "SoundGroupId");
            AddForeignKey("dbo.SoundGroups", "LibraryId", "dbo.Libraries", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Sounds", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Categories", "SoundGroupId", "dbo.SoundGroups", "Id", cascadeDelete: true);
        }
    }
}

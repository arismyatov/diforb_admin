// <auto-generated />
namespace Diforb.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class librayAudioSrc : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(librayAudioSrc));
        
        string IMigrationMetadata.Id
        {
            get { return "201601181414393_librayAudioSrc"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

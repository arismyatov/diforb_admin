namespace Diforb.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class librayAuthor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Libraries", "Author", c => c.String());
            AddColumn("dbo.Libraries", "CareatorId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Libraries", "CareatorId");
            DropColumn("dbo.Libraries", "Author");
        }
    }
}

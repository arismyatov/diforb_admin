﻿using System;
using Diforb.DAL.Classes;

namespace Diforb.DAL
{
    public class UnitOfWork : IDisposable
    {
        private Repository<Library> _libraryRepository;
        private Repository<SoundGroup> _soundGroupRepository;
        private Repository<Category> _categoryRepository;
        private Repository<Sound> _soundRepository;
        private Repository<File> _fileRepository;
        private Repository<Subscriber> _subscriberRepository;

        public DiforbContext DbContext { get; private set; }

        public Repository<Library> LibraryRepository
        {
            get { return _libraryRepository ?? (_libraryRepository = new Repository<Library>(DbContext)); }
        }

        public Repository<SoundGroup> SoundGroupRepository
        {
            get { return _soundGroupRepository ?? (_soundGroupRepository = new Repository<SoundGroup>(DbContext)); }
        }

        public Repository<Category> CategoryRepository
        {
            get { return _categoryRepository ?? (_categoryRepository = new Repository<Category>(DbContext)); }
        }

        public Repository<Sound> SoundRepository
        {
            get { return _soundRepository ?? (_soundRepository = new Repository<Sound>(DbContext)); }
        }

        public Repository<File> FileRepository
        {
            get { return _fileRepository ?? (_fileRepository = new Repository<File>(DbContext)); }
        }

        public Repository<Subscriber> SubscriberRepository
        {
            get { return _subscriberRepository ?? (_subscriberRepository = new Repository<Subscriber>(DbContext)); }
        }


        public void Save()
        {
            DbContext.SaveChanges();
        }

        private bool _disposed;

        public UnitOfWork()
        {
            DbContext = new DiforbContext();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DbContext.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

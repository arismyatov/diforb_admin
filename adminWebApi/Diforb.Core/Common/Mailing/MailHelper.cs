using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace Diforb.Core.Common.Mailing
{
    public class MailHelper
    {
        public static string DiforbInfoEmail = ConfigurationManager.AppSettings["DiforbInfoEmail"];
        private readonly string _fromName = ConfigurationManager.AppSettings["MailFrom"];
        private readonly string _fromAddress = ConfigurationManager.AppSettings["MailFromEmail"];
        private readonly string _senderName;
        private readonly string _senderAddress;
        private readonly int _smtpPort;
        private readonly List<string> _toEmails;
        private readonly List<string> _toCcEmails;
        private readonly List<string> _toBccEmails;
        private readonly string _emailSubject = ConfigurationManager.AppSettings["MailSubject"];
        private readonly bool _isBodyHtml;
        private readonly string _emailBody;
        private readonly List<AlternateView> _alternateViews;
        private readonly byte[] _attachmentFile;
        private readonly string _attachmentName;
        private const MailPriority Priority = MailPriority.Normal;


        public MailHelper(string fromName, string fromAddress, List<string> toEmails, List<string> toCcEmails,
                          List<string> toBccEmails, string emailSubject, bool isBodyHtml, string emailBody,
                          List<AlternateView> alternateViews, string senderName, string senderAddress, int smtpPort, byte[] attachmentFile, string attachmentName)
        {
            _fromName = fromName;
            _fromAddress = fromAddress;
            _toEmails = toEmails;
            _toCcEmails = toCcEmails;
            _toBccEmails = toBccEmails;
            _emailSubject = emailSubject;
            _isBodyHtml = isBodyHtml;
            _emailBody = emailBody;
            _alternateViews = alternateViews;
            _senderName = senderName;
            _senderAddress = senderAddress;
            _smtpPort = smtpPort;
            _attachmentFile = attachmentFile;
            _attachmentName = attachmentName;
        }

        public MailHelper(List<string> toEmails,  bool isBodyHtml,
                          string emailBody,  string senderName, 
                          string senderAddress, byte[] attachmentFile, string attachmentName, 
                          List<string> toCcEmails = null, List<string> toBccEmails = null,
                          List<AlternateView> alternateViews = null)
        {
            _toEmails = toEmails;
            _toCcEmails = toCcEmails;
            _toBccEmails = toBccEmails;
            _isBodyHtml = isBodyHtml;
            _emailBody = emailBody;
            _alternateViews = alternateViews;
            _senderName = senderName;
            _senderAddress = senderAddress;
            _attachmentFile = attachmentFile;
            _attachmentName = attachmentName;
        }

        public MailHelper(List<string> toEmails, List<string> toCcEmails, List<string> toBccEmails, bool isBodyHtml,
                          string emailBody, List<AlternateView> alternateViews)
        {
            _toEmails = toEmails;
            _toCcEmails = toCcEmails;
            _toBccEmails = toBccEmails;
            _isBodyHtml = isBodyHtml;
            _emailBody = emailBody;
            _alternateViews = alternateViews;
        }

        public MailHelper(string emailBody)
        {
            _isBodyHtml = true;
            _toEmails = new List<string> { DiforbInfoEmail } ;
            _emailBody = emailBody;
        }

        public string SendMessage()
        {
            var response = "";
            var message = new MailMessage();

            message.Headers.Add("diforb", "info");
            message.IsBodyHtml = _isBodyHtml;
            message.Priority = Priority;
            var from = GetMailAddressList(new List<string> {_fromAddress});
            message.From = new MailAddress(from[0].Address, _fromName);

            if (_senderAddress != null)
            {
                var sender = GetMailAddressList(new List<string> {_senderAddress});
                message.Sender = new MailAddress(sender[0].Address, _senderName);
            }

            if (_attachmentFile != null)
            {
                if (_attachmentFile.Length > 0)
                {
                    var data = new Attachment(new MemoryStream(_attachmentFile),
                                              (!string.IsNullOrEmpty(_attachmentName))
                                                  ? _attachmentName
                                                  : "attachment.png");
                    message.Attachments.Add(data);
                }
            }

            var listToEmails = GetMailAddressList(_toEmails);
            foreach (var email in listToEmails)
            {
                message.To.Add(email);
            }

            if (_toCcEmails != null)
            {
                var listCcEmails = (GetMailAddressList(_toCcEmails));
                foreach (var email in listCcEmails)
                {
                    message.CC.Add(email);
                }
            }

            if (_toBccEmails != null)
            {
                var listBccEmails = (GetMailAddressList(_toBccEmails));
                foreach (var email in listBccEmails)
                {
                    message.Bcc.Add(email);
                }
            }

            message.Subject = _emailSubject;

            if (_alternateViews != null)
            {
                foreach (var alternateView in _alternateViews)
                {
                    message.AlternateViews.Add(alternateView);
                }
            }
            else
            {
                message.Body = _emailBody;
            }

            var client = new SmtpClient();

            using (client)
            {
                if (message.To.Count != 0)
                {
                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception ex)
                    {
                        response = ex.Message;
                    }
                }
                else
                {
                    response = "Emails can't be empty!";
                }               
            }
            
            Thread.Sleep(1000);
            return response;
        }

        private static List<MailAddress> GetMailAddressList(IEnumerable<string> emailList)
        {
            return emailList.Where(x => !string.IsNullOrEmpty(x)).Select(z => new MailAddress(z)).ToList();
        }

        public static string CreateHtmlMessage<T>(T result, string xlstPath, bool isWeb)
        {
            var xmlSerializer = new XmlSerializer(typeof (T));
            var xmlReader =
                XmlReader.Create(isWeb
                                     ? HostingEnvironment.VirtualPathProvider.GetFile(xlstPath).Open()
                                     : new StreamReader(xlstPath).BaseStream);
            var transform = new XslCompiledTransform();
            transform.Load(xmlReader);

            var doc = new XmlDocument();
            var stream = new MemoryStream();
            stream.Seek(0, SeekOrigin.Begin);
            xmlSerializer.Serialize(stream, result);
            stream.Seek(0, SeekOrigin.Begin);
            doc.Load(stream);

            var sb = new StringBuilder();
            var sw = new StringWriter(sb);

            var args = new XsltArgumentList();
            transform.Transform(doc, args, sw);

            return sw.ToString();
        }

    }

}
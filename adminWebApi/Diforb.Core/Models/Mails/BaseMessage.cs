﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diforb.Core.Models.Mails
{
    public abstract class BaseMessage
    {
        public List<string> ToEmails { set; get; }
        public List<string> ToCcEmails { set; get; }
        public List<string> ToBccEmails { set; get; }
        public string LetterLink { set; get; }
    }
}

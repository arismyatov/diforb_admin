﻿
namespace Diforb.Core.Models.Mails
{
    public class SubscriberMessage : BaseMessage
    {
        public string Email { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using Diforb.Core.Common.Mailing;
using Diforb.Core.Models.Mails;

namespace Dforb.Services.Sevices
{
    public class Mails
    {
        public void SendWellcomeToSubscriber(string email)
        {
            var emails = new List<SubscriberMessage>
            {
                new SubscriberMessage { Email = email }
            };

            var text = MailHelper.CreateHtmlMessage(emails, "~/App_Data/MailTemplates/WellcomeToSubscriber.xslt", true);
            var helper = new MailHelper(new List<string> { email }, null, null, true, text, null);
            helper.SendMessage();
        }

        public void SendNewSubscriber(string email)
        {
            var emails = new List<SubscriberMessage>
            {
                new SubscriberMessage { Email = email }
            };

            var text = MailHelper.CreateHtmlMessage(emails, "~/App_Data/MailTemplates/NewSubscriber.xslt", true);
            var helper = new MailHelper(text);
            helper.SendMessage();
        }

    }
}

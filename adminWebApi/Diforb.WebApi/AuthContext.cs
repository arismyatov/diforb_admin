﻿using System.Data.Entity;
using Diforb.WebApi.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Diforb.WebApi
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("DiforbAuthConnection")
        {
     
        }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

    }

}
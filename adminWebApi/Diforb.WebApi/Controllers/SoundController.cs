﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Diforb.DAL;
using Diforb.DAL.Classes;
using Diforb.WebApi.DTOs;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Diforb.WebApi.Controllers
{
    [RoutePrefix("api/sound")]
    [Authorize]
    public class SoundController : ApiController
    {
        readonly UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Sound
        public IHttpActionResult Get()
        {
            var sounds = _unitOfWork.SoundRepository.Get(includeProperties: "Categories");
            if (sounds == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }

            var soundsDto = sounds.Select(l => (SoundDto) l);
            var soundDtos = soundsDto as SoundDto[] ?? soundsDto.ToArray();
            foreach (var sound in soundDtos)
            {
                sound.Library = _unitOfWork.LibraryRepository.GetById(sound.LibraryId).Title;
            }

            return Ok(soundDtos);
        }

        // GET: api/Sound/5
        public SoundDto Get(Guid id)
        {
            var sound = _unitOfWork.SoundRepository.Get(includeProperties: "Categories", filter: c => c.Id == id).FirstOrDefault();
            if (sound == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            var soundDto = (SoundDto) sound;
            soundDto.Library = _unitOfWork.LibraryRepository.GetById(soundDto.LibraryId).Name;

            return soundDto;      
        }

        // POST: api/Sound
        public IHttpActionResult Post(Sound sound)
        {
            //if (!ModelState.IsValid)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            //}
            var categories = sound.Categories.Select(c => new Category {Id = c.Id}).ToList();
            categories.ForEach(c => _unitOfWork.DbContext.Categories.Attach(c));
            sound.Categories.Clear();
            categories.ForEach(c => sound.Categories.Add(c));

            sound.Id = Guid.NewGuid();
            _unitOfWork.SoundRepository.Insert(sound);
            _unitOfWork.Save();
            var response = Request.CreateResponse(HttpStatusCode.Created, sound);
            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = sound.Id }));

            return StatusCode(HttpStatusCode.Created);
        }

        // PUT: api/Sound/5
        public IHttpActionResult Put(Sound sound)
        {
            var soundInDb =
                _unitOfWork.SoundRepository.Get(c => c.Id == sound.Id, includeProperties: "Categories")
                    .Single();

            _unitOfWork.DbContext.Entry(soundInDb).CurrentValues.SetValues(sound);

            // Remove Sound Groups
            foreach (
                var category in
                    soundInDb.Categories.ToList()
                        .Where(category => sound.Categories.All(ct => ct.Id != category.Id)))
                soundInDb.Categories.Remove(category);

            // Add Sound Groups
            foreach (
                var category in
                    sound.Categories.ToList()
                        .Where(category => soundInDb.Categories.All(ct => ct.Id != category.Id)))
            {
                _unitOfWork.DbContext.Categories.Attach(category);
                soundInDb.Categories.Add(category);
            }

            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return StatusCode(HttpStatusCode.ExpectationFailed);
            }

            return Ok();
        }

        // DELETE: api/Sound/5
        public HttpResponseMessage Delete(Guid id)
        {
            var sound = _unitOfWork.SoundRepository.GetById(id);
            if (sound == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            _unitOfWork.SoundRepository.Delete(sound);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, sound);
        }
    }
}

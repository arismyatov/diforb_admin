﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Diforb.DAL;
using Diforb.DAL.Classes;
using Diforb.DAL.Classes.Enums;
using Diforb.WebApi.DTOs;
using ClientApp = Diforb.WebApi.DTOs.ClientApp;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Diforb.WebApi.Controllers
{
    [RoutePrefix("api/library")]
    public class LibraryController : ApiController
    {
        readonly UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Library
        [Authorize]
        public IHttpActionResult Get()
        {
            var libraries = _unitOfWork.LibraryRepository.Get();

            return Ok(libraries.Select(l => (LibraryDto)l));
        }

        [Route("{id:guid}/sides")]
        public IHttpActionResult GetSides(Guid id)
        {
            try
            {
                return Ok(GetLibSides(id));
            }
            catch (Exception)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }

        [Route("descriptions")]
        public IHttpActionResult GetLibDescriptions()
        {
            try
            {
                var libraries = _unitOfWork.LibraryRepository.Get(includeProperties: "Categories");
                return Ok(libraries.Select(l => (LibraryDescriptionDto)l));
            }
            catch (Exception)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }


        [Route("name/{libName}/sides")]
        public IHttpActionResult GetSidesByName(string libName)
        {
            var library = _unitOfWork.LibraryRepository.Get(l => l.Name == libName).FirstOrDefault();
            if (library == null)
            {
                return NotFound();
            }

            return Ok(GetLibSides(library.Id));
        }


        // GET: api/Library/5
        [Authorize]
        public Library Get(Guid id)
        {
            var library = _unitOfWork.LibraryRepository.GetById(id);
            if (library == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return library;
        }

        // POST: api/Library
        [Authorize]
        public HttpResponseMessage Post(Library library)
        {
            if (ModelState.IsValid)
            {
                library.Id = Guid.NewGuid();
                _unitOfWork.LibraryRepository.Insert(library);
                _unitOfWork.Save();
                var response = Request.CreateResponse(HttpStatusCode.Created, library);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = library.Id }));
                return response;
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT: api/Library/5
        [Authorize]
        public HttpResponseMessage Put(Library library)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            //if (id != library.Id)
            //{
            //    return Request.CreateResponse(HttpStatusCode.BadRequest);
            //}
            _unitOfWork.LibraryRepository.Update(library);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE: api/Library/5
        [Authorize]
        public HttpResponseMessage Delete(Guid id)
        {
            var library = _unitOfWork.LibraryRepository.GetById(id);
            if (library == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            _unitOfWork.LibraryRepository.Delete(library);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, library);
        }


        private List<ClientApp.Library> GetLibSides(Guid libId)
        {
            var lib = (ClientApp.Library)_unitOfWork.LibraryRepository.GetById(libId);

            var leftSoundGroups = _unitOfWork.SoundGroupRepository.Get(
                sg => sg.LibraryId == libId && sg.Side == Side.Left, includeProperties: "Categories");
            var groups = leftSoundGroups as IList<SoundGroup> ?? leftSoundGroups.ToList();
            foreach (var soundGroup in groups)
            {
                foreach (var category in soundGroup.Categories)
                {
                    var catId = category.Id;
                    category.Categories = _unitOfWork.CategoryRepository.Get(c => c.ParentCategoryId == catId, includeProperties: "Sounds").ToList();
                }
            }

            var rightSoundGroups = _unitOfWork.SoundGroupRepository.Get(
                sg => sg.LibraryId == libId && sg.Side == Side.Right, includeProperties: "Categories");

            var rightSoundGroups1 = rightSoundGroups as SoundGroup[] ?? rightSoundGroups.ToArray();
            foreach (var soundGroup in rightSoundGroups1)
            {
                foreach (var category in soundGroup.Categories)
                {
                    var catId = category.Id;
                    category.Categories = _unitOfWork.CategoryRepository.Get(c => c.ParentCategoryId == catId, includeProperties: "Sounds").ToList();
                }
            }

            var leftSide = new ClientApp.Side
            {
                IsLeft = true
            };
            var rightSide = new ClientApp.Side
            {
                IsRight = true
            };

            IEnumerable<SoundGroup> soundGroups = leftSoundGroups as SoundGroup[] ?? groups.ToArray();
            leftSide.SoundGroups = soundGroups.Select(sg => (ClientApp.SoundGroup)sg).AsQueryable();
            //leftSide.SoundGroups.ForEach(sg => SetSide(sg.Categories, Side.Left.ToString(), sg.Name));

            var enumerable = rightSoundGroups as SoundGroup[] ?? rightSoundGroups1.ToArray();
            rightSide.SoundGroups = enumerable.Select(sg => (ClientApp.SoundGroup)sg).AsQueryable();
            //rightSide.SoundGroups.ForEach(sg => SetSide(sg.Categories, Side.Right.ToString(), sg.Name));

            lib.Sides = new List<ClientApp.Side> { leftSide, rightSide };

            return new List<ClientApp.Library> { lib };
        }



        //private static void SetSide(List<ClientApp.Category> categories, string side, string soundGroupName)
        //{
        //    if (categories == null || !categories.Any())
        //    {
        //        return;
        //    }

        //    foreach (
        //        var category in
        //            categories.Where(category => category.SubCategory != null && category.SubCategory.Any()))
        //    {
        //        SetSide(category.SubCategory, side, soundGroupName);
        //    }

        //    foreach (var category in categories.Where(category => category.Sounds != null && category.Sounds.Any()))
        //    {
        //        category.Sounds.ForEach(s =>
        //        {
        //            s.Side = side;
        //            s.CategoryId = category.Id;
        //            s.SoundGroupName = soundGroupName;
        //        });
        //    }
        //}




    }
}

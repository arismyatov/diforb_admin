﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Diforb.DAL;
using Diforb.DAL.Classes;
using Diforb.WebApi.BlobStorage;
using Diforb.WebApi.DTOs;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Diforb.WebApi.Controllers
{
    [RoutePrefix("api/file")]
    // [Authorize]
    public class FileController : ApiController
    {
        readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private IBlobStorageHelper blobStorageHelper;
        private readonly IBlobService _blobService = new BlobService();

        public FileController()
            : this(new AzureFileManager(CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["DiforbStorage"].ConnectionString), "sounds"))
        {
        }

        public FileController(IBlobStorageHelper blobStorageHelper)
        {
            this.blobStorageHelper = blobStorageHelper;
        }

        // GET: api/File
        public IHttpActionResult Get()
        {
            var files = _unitOfWork.FileRepository.Get(includeProperties: "Category,Sound");
            var filesArr = files as File[] ?? files.ToArray();
            foreach (var file in filesArr)
            {
                file.Category.Library = _unitOfWork.LibraryRepository.GetById(file.Category.LibraryId);
            }

            return Json(filesArr.Select(l => (FileDto) l),
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> {new StringEnumConverter()}
                });
        }


        public IHttpActionResult Get(Guid id)
        {
            File file = null;
            try
            {
                file = _unitOfWork.FileRepository.GetById(id);
            }
            catch (Exception)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }
            
            if (file == null)
            {
                return StatusCode(HttpStatusCode.NotFound); 
            }
            return Ok(file);
        }

        // GET: api/File/5
        [Route("buffer/{categoryId:guid}/{soundId:guid}")]
        public async Task<HttpResponseMessage> GetBuffer(Guid categoryId, Guid soundId)
        {
            var file =
                _unitOfWork.FileRepository.Get(f => f.CategoryId == categoryId && f.SoundId == soundId)
                    .FirstOrDefault();
            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);                
            }
            var result = await _blobService.DownloadBlob(file.PathWithName);
            if (result == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            result.BlobStream.Position = 0;
            var message = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(result.BlobStream)
            };

            message.Content.Headers.ContentLength = result.BlobLength;
            message.Content.Headers.ContentType = new MediaTypeHeaderValue(result.BlobContentType);
            message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = HttpUtility.UrlDecode(result.BlobFileName),
                Size = result.BlobLength
            };
            return message;
        }



        // GET: api/File/5
        [Route("bufferbyid/{fileId:guid}")]
        public async Task<HttpResponseMessage> GetBufferById(Guid fileId)
        {
            var file =
                _unitOfWork.FileRepository.Get(f => f.Id == fileId)
                    .FirstOrDefault();
            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            var result = await _blobService.DownloadBlob(file.PathWithName);
            if (result == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            result.BlobStream.Position = 0;
            var message = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(result.BlobStream)
            };

            message.Content.Headers.ContentLength = result.BlobLength;
            message.Content.Headers.ContentType = new MediaTypeHeaderValue(result.BlobContentType);
            message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = HttpUtility.UrlDecode(result.BlobFileName),
                Size = result.BlobLength
            };
            return message;
        }


        // GET: api/File/5
        [Route("list/{categoryId:guid}/{soundId:guid}")]
        public IHttpActionResult GetFileList(Guid categoryId, Guid soundId)
        {
            var fileList =
                _unitOfWork.FileRepository.Get(f => f.CategoryId == categoryId && f.SoundId == soundId)
                    .Select(f => new {id = f.Id});
            return Ok(fileList);
        }


        // POST: api/File
        public async Task<IHttpActionResult> Post()
        {

            try
            {
                // This endpoint only supports multipart form data
                if (!Request.Content.IsMimeMultipartContent("form-data"))
                {
                    return StatusCode(HttpStatusCode.UnsupportedMediaType);
                }

                try
                {
                    // Call service to perform upload, then check result to return as content
                    await _blobService.GetFromContext(Request.Content);
                    if (_blobService.FormData == null)
                    {
                        return BadRequest();
                    }

                    var serializer = new JavaScriptSerializer();
                    var newFile =  (File)serializer.Deserialize(_blobService.FormData["model"], typeof(File));

                    if (newFile == null ||
                        newFile.CategoryId == Guid.Empty)
                    {
                        return BadRequest();                       
                    }

                    var category = _unitOfWork.CategoryRepository.Get(filter: c => c.Id == newFile.CategoryId, includeProperties: "ParentCategory,Library").FirstOrDefault();
                    //var sound = _unitOfWork.SoundRepository.Get(c => c.Id == newFile.SoundId).FirstOrDefault();

                    // var category = _unitOfWork.CategoryRepository.GetById(newFile.CategoryId);
                    if (category == null)
                    {
                        return BadRequest();
                    }

                    // Get Path to file
                    var pathToFile = new StringBuilder();
                    pathToFile.Append(category.Library != null ? category.Library.Name : string.Empty);
                    pathToFile.Append("/");
                    pathToFile.Append(GetFullpath(category));

                    //Upload Sound Blob
                    var result = _blobService.UploadFiles(pathToFile.ToString());

                    //Set fileds to file entity
                    newFile.PathWithName = result[0].FileName;
                    newFile.Id = Guid.NewGuid();

                    //Save file sound entity
                    _unitOfWork.FileRepository.Insert(newFile);
                    _unitOfWork.Save();                   
                }
                catch (Exception ex)
                {
                    return BadRequest(); 
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }







            //if (!Request.Content.IsMimeMultipartContent("form-data"))
            //{
            //    return BadRequest("Unsupported media type");
            //}

            //try
            //{
            //    var files = await blobStorageHelper.Add(Request);
            //    return Ok(new { Successful = true, Message = "Files uploaded ok", Files = files });
            //}
            //catch (Exception ex)
            //{
            //    return BadRequest(ex.GetBaseException().Message);
            //}


            //if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.OK, file);

            //try
            //{
            //    var category = _unitOfWork.CategoryRepository.GetById(file.CategoryId);
            //    file.PathWithName = GetPathToFile(category, "Steps_Cobblestones_Men_Barefoot_01.wav");
            //    file.Id = Guid.NewGuid();
            //    _unitOfWork.FileRepository.Insert(file);
            //    _unitOfWork.Save();
            //}
            //catch (Exception ex)
            //{
            //    return Request.CreateResponse(HttpStatusCode.InternalServerError);
            //}

            //return Request.CreateResponse(HttpStatusCode.OK, new {pathWithName = file.PathWithName});
        }


        //// POST: api/File
        //public HttpResponseMessage Post(File file)
        //{

        //    if (!Request.Content.IsMimeMultipartContent("form-data"))
        //    {
        //        return BadRequest("Unsupported media type");
        //    }

        //    try
        //    {
        //        var files = await blobStorageHelper.Add(Request);
        //        return Ok(new { Successful = true, Message = "Files uploaded ok", Files = files });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.GetBaseException().Message);
        //    }


        //    if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.OK, file);

        //    try
        //    {
        //        var category = _unitOfWork.CategoryRepository.GetById(file.CategoryId);
        //        file.PathWithName = GetPathToFile(category, "Steps_Cobblestones_Men_Barefoot_01.wav");
        //        file.Id = Guid.NewGuid();
        //        _unitOfWork.FileRepository.Insert(file);
        //        _unitOfWork.Save();
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, new { pathWithName = file.PathWithName });
        //}


        // PUT: api/File/5
        public HttpResponseMessage Put(Guid id, File file)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            if (id != file.Id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            _unitOfWork.FileRepository.Update(file);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE: api/File/5
        public async Task<IHttpActionResult> Delete(Guid id)
        {
            var file = _unitOfWork.FileRepository.GetById(id);
            if (file == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            // Delete blob
            var result = await _blobService.DeleteBlob(file.PathWithName);
            if (!result)
            {
                return StatusCode(HttpStatusCode.ExpectationFailed);
            }

            _unitOfWork.FileRepository.Delete(file);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            return StatusCode(HttpStatusCode.OK);
        }


        // private methods

        private string GetPathToFile(Category category)
        {
            var filePath = new StringBuilder(GetFullpath(category));
            return filePath.ToString().ToLower();
        }

        private static string GetFullpath(Category category)
        {
            if (category.ParentCategory == null) return category.Title;
            var name = new StringBuilder();
            name.Append(GetFullpath(category.ParentCategory));
            name.Append("/");
            name.Append(category.Title);
            return name.ToString();
        }


    }
}
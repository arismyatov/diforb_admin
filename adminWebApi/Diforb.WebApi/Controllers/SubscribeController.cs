﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using Diforb.DAL;
using Diforb.DAL.Classes;
using Diforb.WebApi.DTOs;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Dforb.Services.Sevices;

namespace Diforb.WebApi.Controllers
{
    [RoutePrefix("api/subscribe")]
    public class SubscribeController : ApiController
    {
        readonly UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Subscriber
        public IHttpActionResult Get()
        {
            var subscribers = _unitOfWork.SubscriberRepository.Get();
            return Json(subscribers.Select(l => (SubscriberDto)l), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter> { new StringEnumConverter() } });
        }

        // POST: api/Subscriber
        public HttpResponseMessage Post(Subscriber subscriber)
        {
            if (ModelState.IsValid)
            {
                subscriber.Id = Guid.NewGuid();
                _unitOfWork.SubscriberRepository.Insert(subscriber);
                _unitOfWork.Save();

                // Send Email to subscriber

                //Mial to us 

                var mailingSystem = new Mails();
                mailingSystem.SendWellcomeToSubscriber(subscriber.Email);
                mailingSystem.SendNewSubscriber(subscriber.Email);



                //sendEmail("Новая подписка", string.Format("<p> Получена новая заявка на рассылку.  Email: </p> <p>{0}</p> ", subscriber.Email), "i@diforb.com");
                ////Mial to subscriber
                //sendEmail("Diforb news", string.Format("<p>Благодарим за подписку!</p> <p>О всех наших новостях Вы узнаете первыми!</p> <p>С уважением, команда Diforb.</p>"), subscriber.Email);

                var response = Request.CreateResponse(HttpStatusCode.Created, subscriber);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = subscriber.Id }));
                return response;
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // DELETE: api/Category/5
        public HttpResponseMessage Delete(Guid id)
        {
            var category = _unitOfWork.CategoryRepository.GetById(id);
            if (category == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            _unitOfWork.CategoryRepository.Delete(category);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, category);
        }

        private void sendEmail(string subject, string body, string mailTo)
        {
            // Include this.         
            //var userName = "i@diforb.com";
            const string fromAddress = "i@diforb.com";
            //var mailPassword = "Diforb_Sound_2014";

            // Create smtp connection.
            var message = new MailMessage { From = new MailAddress(fromAddress) };
            message.To.Add(new MailAddress(mailTo)); 
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;


            using (var smtp = new SmtpClient())
            {
                smtp.Send(message);
            }
        }
    }
}

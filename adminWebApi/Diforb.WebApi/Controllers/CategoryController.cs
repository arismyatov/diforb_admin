﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Diforb.DAL;
using Diforb.DAL.Classes;
using Diforb.WebApi.DTOs;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Diforb.WebApi.Controllers
{
    [RoutePrefix("api/category")]
    [Authorize]
    public class CategoryController : ApiController
    {
        readonly UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Category
        public IHttpActionResult Get()
        {
            var categories = _unitOfWork.CategoryRepository.Get(includeProperties: "Library,SoundGroups");
            return Json(categories.Select(l => (CategoryDto)l), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter> { new StringEnumConverter() } });
        }

        // GET: api/Category
        //[HttpGet]
        //public IHttpActionResult GetByLibrary(Guid id)
        //{
        //    var categories = _unitOfWork.CategoryRepository.Get(includeProperties: "Library,ParentCategory", filter: c => c.LibraryId == id);
        //    return Json(categories.Select(l => (CategoryDto)l), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter> { new StringEnumConverter() } });
        //}

        //ToDo: Implement getting Ctegories by LibraryId
        //// GET: api/Category/GetByLibrary/id
        //public IHttpActionResult GetByLibrary(Guid id)
        //{
        //    var categories = _unitOfWork.CategoryRepository.Get(c => c.LibraryId.Equals(id), includeProperties: "Library");
        //    return Json(categories.Select(l => (CategoryDto)l), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter> { new StringEnumConverter() } });
        //}

        // GET: api/Category/5
        public CategoryDto Get(Guid id)
        {
            var category = _unitOfWork.CategoryRepository.Get(includeProperties: "SoundGroups", filter: c => c.Id == id).FirstOrDefault();
            if (category == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return category;
        }

        // POST: api/Category
        public HttpResponseMessage Post(Category category)
        {
            //if (!ModelState.IsValid)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            //}

            if (category.SoundGroups != null)
            {
                var soundGroups = category.SoundGroups.Select(c => new SoundGroup { Id = c.Id }).ToList();
                soundGroups.ForEach(c => _unitOfWork.DbContext.SoundGroups.Attach(c));
                category.SoundGroups.Clear();
                soundGroups.ForEach(c => category.SoundGroups.Add(c));
            }

            category.Id = Guid.NewGuid();
            _unitOfWork.CategoryRepository.Insert(category);
            _unitOfWork.Save();

            var response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = category.Id }));
            return response;
        }

        // PUT: api/Category/5
        public HttpResponseMessage Put(Category category)
        {
            var categoryInDb =
                _unitOfWork.CategoryRepository.Get(c => c.Id == category.Id, includeProperties: "SoundGroups")
                    .Single();

            _unitOfWork.DbContext.Entry(categoryInDb).CurrentValues.SetValues(category);

            // Remove Sound Groups
            foreach (
                var soundGroup in
                    categoryInDb.SoundGroups.ToList()
                        .Where(soundGroup => category.SoundGroups.All(sg => sg.Id != soundGroup.Id)))
                categoryInDb.SoundGroups.Remove(soundGroup);

            // Add Sound Groups
            foreach (
                var soundGroup in
                    category.SoundGroups.ToList()
                        .Where(soundGroup => categoryInDb.SoundGroups.All(sg => sg.Id != soundGroup.Id)))
            {
                _unitOfWork.DbContext.SoundGroups.Attach(soundGroup);
                categoryInDb.SoundGroups.Add(soundGroup);
            }




            /*
            var origSoundGroups = _unitOfWork.CategoryRepository.Get(filter: c => c.Id == category.Id, includeProperties: "SoundGroups").First().SoundGroups;
            if (category.SoundGroups != null)
            {
                _unitOfWork.DbContext.Categories.Attach(category);
                var soundGroups = category.SoundGroups.Select(c => new SoundGroup { Id = c.Id }).ToList();
                soundGroups.ForEach(c => _unitOfWork.DbContext.SoundGroups.Attach(c));
                // category.SoundGroups.Clear();
                if (category.SoundGroups != null)
                {
                    // origCategory.SoundGroups.Clear();                   
                    category.SoundGroups.Clear();
                }
                else
                {
                    category.SoundGroups = new Collection<SoundGroup>();
                }
                foreach (
                    var soundGroup in soundGroups.Where(soundGroup => origSoundGroups.All(os => os.Id != soundGroup.Id))
                    )
                {
                    category.SoundGroups.Add(soundGroup);
                }
            }
            */



            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE: api/Category/5
        public HttpResponseMessage Delete(Guid id)
        {
            var category = _unitOfWork.CategoryRepository.GetById(id);
            if (category == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            _unitOfWork.CategoryRepository.Delete(category);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, category);
        }
    }
}

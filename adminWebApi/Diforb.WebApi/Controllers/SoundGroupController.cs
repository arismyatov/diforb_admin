﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Diforb.DAL;
using Diforb.DAL.Classes;
using Diforb.WebApi.DTOs;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Diforb.WebApi.Controllers
{
    [RoutePrefix("api/soundgroup")]
    [Authorize]
    public class SoundGroupController : ApiController
    {
        readonly UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/soundGroup
        public IHttpActionResult Get()
        {
            var soundGroups = _unitOfWork.SoundGroupRepository.Get(null, null, "Library");
            return Json(soundGroups.Select(l => (SoundGroupDto)l), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter> { new StringEnumConverter() } });
        }

        // GET: api/soundGroup/5
        public SoundGroup Get(Guid id)
        {
            var soundGroup = _unitOfWork.SoundGroupRepository.GetById(id);
            if (soundGroup == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return soundGroup;
        }

        // POST: api/soundGroup
        public HttpResponseMessage Post(SoundGroup soundGroup)
        {
            if (ModelState.IsValid)
            {
                soundGroup.Id = Guid.NewGuid();
                _unitOfWork.SoundGroupRepository.Insert(soundGroup);
                _unitOfWork.Save();
                var response = Request.CreateResponse(HttpStatusCode.Created, soundGroup);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = soundGroup.Id }));
                return response;
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT: api/soundGroup/5
        public HttpResponseMessage Put(SoundGroup soundGroup)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            _unitOfWork.SoundGroupRepository.Update(soundGroup);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE: api/soundGroup/5
        public HttpResponseMessage Delete(Guid id)
        {
            var soundGroup = _unitOfWork.SoundGroupRepository.GetById(id);
            if (soundGroup == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            _unitOfWork.SoundGroupRepository.Delete(soundGroup);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, soundGroup);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Diforb.WebApi.BlobStorage.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Diforb.WebApi.BlobStorage
{
    public class AzureFileManager : IBlobStorageHelper
    {
        private CloudStorageAccount StorageAccount {get; set;}
        private string ContainerName { get; set; }

        public string PathToFile { get; set; }
        
        public AzureFileManager(CloudStorageAccount storageAccount, string containerName)
        {
            StorageAccount = storageAccount;
            ContainerName = containerName;
        }        

        public async Task<IEnumerable<SoundFileModel>> Get()
        {
            //note the browser will get the actual images directly from the container we are not passing actual files back just references
            var blobClient = StorageAccount.CreateCloudBlobClient();
            var soundContainer = blobClient.GetContainerReference(ContainerName);

            if (! await soundContainer.ExistsAsync())
            {
                await soundContainer.CreateAsync(BlobContainerPublicAccessType.Blob, null, null);                
            }

            var sounds = new List<SoundFileModel>();
            var blobItems = soundContainer.ListBlobs();

            foreach (var listBlobItem in blobItems.Where(bi => bi is CloudBlockBlob))
            {
                var blobItem = (CloudBlockBlob) listBlobItem;
                await blobItem.FetchAttributesAsync();

                if (blobItem.Properties.LastModified != null)
                    sounds.Add(new SoundFileModel
                    {
                        Name = blobItem.Name,
                        Size = blobItem.Properties.Length / 1024,
                        Created = blobItem.Metadata["Created"] == null ? DateTime.Now : DateTime.Parse(blobItem.Metadata["Created"]),
                        Modified = ((DateTimeOffset)blobItem.Properties.LastModified).DateTime,
                        Url = blobItem.Uri.AbsoluteUri
                    });
            }

            return sounds;
        }

        public async Task<BlobStorageActionResult> Delete(string fileName)
        {
            var blobClient = StorageAccount.CreateCloudBlobClient();
            var soundContainer = blobClient.GetContainerReference(ContainerName);

            try
            {
                var blob = await soundContainer.GetBlobReferenceFromServerAsync(fileName);
                await blob.DeleteAsync();

                return new BlobStorageActionResult { Successful = true, Message = fileName + "deleted successfully" };
            }
            catch(Exception ex)
            {
                return new BlobStorageActionResult { Successful = false, Message = "error deleting fileName " + ex.GetBaseException().Message };
            }
        }

        public async Task<IEnumerable<SoundFileModel>> Add(HttpRequestMessage request)
        {
            var blobClient = StorageAccount.CreateCloudBlobClient();
            var soundContainer = blobClient.GetContainerReference(ContainerName);

            var provider = new AzureBlobMultipartFormDataStreamProvider(soundContainer);

            await request.Content.ReadAsMultipartAsync(provider);

            //await request.Content.ReadAsMultipartAsync(provider);

            var sounds = new List<SoundFileModel>();

            foreach (var file in provider.FileData)
            {
                //the LocalFileName is going to be the absolute Uri of the blob (see GetStream)
                //use it to get the blob info to return to the client
                var blob = await soundContainer.GetBlobReferenceFromServerAsync(PathToFile);
                await blob.FetchAttributesAsync();

                if (blob.Properties.LastModified != null)
                    sounds.Add(new SoundFileModel
                    {
                        Name = blob.Name,
                        Size = blob.Properties.Length / 1024,
                        Created = blob.Metadata["Created"] == null ? DateTime.Now : DateTime.Parse(blob.Metadata["Created"]),
                        Modified = ((DateTimeOffset)blob.Properties.LastModified).DateTime,
                        Url = blob.Uri.AbsoluteUri
                    });
            }

            return sounds;      
        }




        //public async Task<IEnumerable<SoundFileModel>> Add(HttpRequestMessage request)
        //{
        //    var blobClient = StorageAccount.CreateCloudBlobClient();
        //    var soundContainer = blobClient.GetContainerReference(ContainerName);

        //    var provider = new AzureBlobMultipartFormDataStreamProvider(soundContainer);

        //    var result = await request.Content.ReadAsMultipartAsync(provider);
        //    //await request.Content.ReadAsMultipartAsync(provider);

        //    var sounds = new List<SoundFileModel>();

        //    foreach (var file in provider.FileData)
        //    {
        //        //the LocalFileName is going to be the absolute Uri of the blob (see GetStream)
        //        //use it to get the blob info to return to the client
        //        var blob = await soundContainer.GetBlobReferenceFromServerAsync(PathToFile);
        //        await blob.FetchAttributesAsync();

        //        if (blob.Properties.LastModified != null)
        //            sounds.Add(new SoundFileModel
        //            {
        //                Name = blob.Name,
        //                Size = blob.Properties.Length / 1024,
        //                Created = blob.Metadata["Created"] == null ? DateTime.Now : DateTime.Parse(blob.Metadata["Created"]),
        //                Modified = ((DateTimeOffset)blob.Properties.LastModified).DateTime,
        //                Url = blob.Uri.AbsoluteUri
        //            });
        //    }

        //    return sounds;
        //}













        public async Task<bool> FileExists(string fileName)
        {
            var blobClient = StorageAccount.CreateCloudBlobClient();
            var soundContainer = blobClient.GetContainerReference(ContainerName);

            var blob = await soundContainer.GetBlobReferenceFromServerAsync(fileName);
            return await blob.ExistsAsync();
        }
    }
}
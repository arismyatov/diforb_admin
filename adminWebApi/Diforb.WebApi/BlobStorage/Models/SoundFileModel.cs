﻿using System;

namespace Diforb.WebApi.BlobStorage.Models
{
    public class SoundFileModel
    {
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public long Size { get; set; }
        public string Url { get; set; }
    }
}

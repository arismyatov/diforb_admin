﻿namespace Diforb.WebApi.BlobStorage
{
    public class BlobStorageActionResult
    {
        public bool Successful { get; set; }
        public string Message { get; set; }
    }
}
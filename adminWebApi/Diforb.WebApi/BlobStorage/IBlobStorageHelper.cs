﻿using System.Collections.Generic;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Diforb.WebApi.BlobStorage.Models;

namespace Diforb.WebApi.BlobStorage
{
    public interface IBlobStorageHelper
    {
        string PathToFile { get; set; }
        Task<IEnumerable<SoundFileModel>> Get();
        Task<BlobStorageActionResult> Delete(string fileName);
        Task<IEnumerable<SoundFileModel>> Add(HttpRequestMessage request);
        Task<bool> FileExists(string fileName);
    }
}

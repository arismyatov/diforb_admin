﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Diforb.WebApi.BlobStorage
{
    public interface IBlobService
    {
        List<BlobUploadModel> UploadFiles(string pathToFile);
        Task<NameValueCollection> GetFromContext(HttpContent httpContent);
        Task<BlobDownloadModel> DownloadBlob(string blobName);
        Task<bool> DeleteBlob(string blobName);      
        NameValueCollection FormData { get; set; }
        Collection<MultipartFileData> FileData { get; set; }
    }



    public class BlobService : IBlobService
    {
        public NameValueCollection FormData { get; set; }
        public List<BlobUploadModel> BlobUploadModels { get; set; }

        public Collection<MultipartFileData> FileData { get; set; }


        public async Task<NameValueCollection> GetFromContext(HttpContent httpContent)
        {
            var blobUploadProvider = new BlobStorageUploadProvider();

            await httpContent.ReadAsMultipartAsync(blobUploadProvider)
                .ContinueWith(task =>
                {
                    if (task.IsFaulted || task.IsCanceled)
                    {
                        throw task.Exception;
                    }

                    var provider = task.Result;
                    return provider.Uploads.ToList();
                });

            // TODO: Use data in the list to store blob info in your
            // database so that you can always retrieve it later.
            FileData = blobUploadProvider.FileData;
            FormData = blobUploadProvider.FormData;

            return FormData;
        }

        public async Task<BlobDownloadModel> DownloadBlob(string blobName)
        {
            if (String.IsNullOrEmpty(blobName)) return null;
            var container = BlobHelper.GetBlobContainer();
            var blob = container.GetBlockBlobReference(blobName);
            var ms = new MemoryStream();
            await blob.DownloadToStreamAsync(ms);

            // Strip off any folder structure so the file name is just the file name
            var lastPos = blob.Name.LastIndexOf('/');
            var fileName = blob.Name.Substring(lastPos + 1, blob.Name.Length - lastPos - 1);

            // Build and return the download model with the blob stream and its relevant info
            var download = new BlobDownloadModel
            {
                BlobStream = ms,
                BlobFileName = fileName,
                BlobLength = blob.Properties.Length,
                BlobContentType = blob.Properties.ContentType
            };
            return download;
        }

        public async Task<bool> DeleteBlob(string blobName)
        {
            if (String.IsNullOrEmpty(blobName)) return false;
            try
            {
                var container = BlobHelper.GetBlobContainer();
                var blob = container.GetBlockBlobReference(blobName);

                blob.Delete();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public List<BlobUploadModel> UploadFiles(string pathToFile)
        {
            var uploads = new List<BlobUploadModel>();
            foreach (var fileData in FileData)
            {
                if (fileData.Headers.ContentDisposition.FileName == null)
                {
                    continue;
                }

                var fileName = Path.GetFileName(fileData.Headers.ContentDisposition.FileName.Trim('"'));
                var azureFileName = string.Format("{0}/{1}", pathToFile, fileName).Replace("-", "_").ToLower();

                // Retrieve reference to a blob
                var blobContainer = BlobHelper.GetBlobContainer();
                var blob = blobContainer.GetBlockBlobReference(azureFileName);

                // Set the blob content type
                blob.Properties.ContentType = fileData.Headers.ContentType.MediaType;

                // Upload file into blob storage, basically copying it from local disk into Azure
                using (var fs = File.OpenRead(fileData.LocalFileName))
                {
                    blob.UploadFromStream(fs);
                }

                // Delete local file from disk
                File.Delete(fileData.LocalFileName);

                // Create blob upload model with properties from blob info

                var blobUpload = new BlobUploadModel
                {
                    FileName = blob.Name.ToLower(),
                    FileUrl = blob.Uri.AbsoluteUri,
                    FileSizeInBytes = blob.Properties.Length
                };

                // Add uploaded blob to the list
                uploads.Add(blobUpload);
            }

            return uploads;
        }
    }
}
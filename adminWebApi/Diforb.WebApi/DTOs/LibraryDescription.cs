﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class LibraryDescriptionDto
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }


        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }


        [JsonProperty(PropertyName = "Order")]
        public int Order { get; set; }


        [JsonProperty(PropertyName = "Image")]
        public string Image { get; set; }


        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }


        [JsonProperty(PropertyName = "Author")]
        public string Author { get; set; }


        [JsonProperty(PropertyName = "Audio")]
        public string Audio { get; set; }


        [JsonProperty(PropertyName = "SoundCount")]
        public int SoundCount { get; set; }


        [JsonProperty(PropertyName = "Categories")]
        public IEnumerable<CategoryDescriptionDto> Categories { get; set; }

        public static implicit operator LibraryDescriptionDto(Library i)
        {
            var libDescription = new LibraryDescriptionDto
            {
                Id = i.Id,
                Title = i.Title,
                Order = i.Order,
                Image = i.ImageSrc,
                Audio = i.AudioSrc,
                Description = i.Description,
                Author = i.Author
            };

            if (!i.Categories.Any())
            {
                return libDescription;
            }

            libDescription.Categories = i.Categories.Select(c => (CategoryDescriptionDto) c);
            

            return libDescription;
        }
    }
}
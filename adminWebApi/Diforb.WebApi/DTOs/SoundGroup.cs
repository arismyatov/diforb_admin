﻿using System;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class SoundGroupDto
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Library")]
        public string Library { get; set; }

        [JsonProperty(PropertyName = "LibraryId")]
        public Guid LibraryId { get; set; }

        [JsonProperty(PropertyName = "Side")]
        public int Side { get; set; }

        public static implicit operator SoundGroupDto(SoundGroup i)
        {
            return new SoundGroupDto
            {
                Id      = i.Id,
                Title   = i.Title,
                Library = i.Library != null ? i.Library.Name : string.Empty,
                LibraryId = i.Library != null ? i.Library.Id : Guid.Empty,
                Side    = (int) i.Side
            };
        }
    }
}
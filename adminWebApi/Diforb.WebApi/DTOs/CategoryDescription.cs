﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class CategoryDescriptionDto
    {
        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Icon")]
        public string Icon { get; set; }

        [JsonProperty(PropertyName = "SoundCount")]
        public int SoundCount { get; set; }

        public static implicit operator CategoryDescriptionDto(Category i)
        {
            return new CategoryDescriptionDto
            {
                Title = i.Title,
                Icon  = i.Icon
            };
        }
    }
}
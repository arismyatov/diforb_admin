﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class FileDto
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Library")]
        public string Library { get; set; }

        [JsonProperty(PropertyName = "Category")]
        public String Category { get; set; }

        [JsonProperty(PropertyName = "Sound")]
        public String Sound { get; set; }

        [JsonProperty(PropertyName = "LibraryId")]
        public Guid LibraryId { get; set; }

        [JsonProperty(PropertyName = "CategoryId")]
        public Guid CategoryId { get; set; }

        [JsonProperty(PropertyName = "SoundId")]
        public Guid SoundId { get; set; }


        public static implicit operator FileDto(File i)
        {
            var soundDto = new FileDto
            {
                Id = i.Id,
                Name = i.PathWithName
            };

            if (i.Category != null)
            {
                soundDto.Category = i.Category.Name;
                soundDto.CategoryId = i.CategoryId;

                soundDto.Library = i.Category.Library.Name;
                soundDto.LibraryId = i.Category.LibraryId;
            }

            if (i.Sound == null) return soundDto;
            soundDto.Sound = i.Sound.Name;
            soundDto.SoundId = i.SoundId;

            return soundDto;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;
using dtos = Diforb.DAL.Classes;

namespace Diforb.WebApi.DTOs.ClientApp
{
    public class Library
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "IconClass")]
        public string IconClass { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Sides")]
        public IEnumerable<Side> Sides { get; set; }


        public static implicit operator Library(dtos.Library i)
        {
            var library = new Library()
            {
                Id = i.Id,
                IconClass = i.Icon,
                Title = i.Title,
                Name = i.Name
            };

            return library;
        }

    }
}
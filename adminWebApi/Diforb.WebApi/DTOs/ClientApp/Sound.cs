﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;
using dtos = Diforb.DAL.Classes;

namespace Diforb.WebApi.DTOs.ClientApp
{
    public class Sound
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Order")]
        public int Order { get; set; }

        [JsonProperty(PropertyName = "CategoryId")]
        public Guid CategoryId { get; set; }

        [JsonProperty(PropertyName = "SoundGroupName")]
        public string SoundGroupName { get; set; }

        [JsonProperty(PropertyName = "Side")]
        public string Side { get; set; }

        public static implicit operator Sound(dtos.Sound i)
        {
            var sound = new Sound
            {
                Id = i.Id,
                Title = i.Title,
                Order = i.Order
            };

            return sound;
        }
    }
}
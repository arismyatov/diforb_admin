﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs.ClientApp
{
    public class Side
    {
        [JsonProperty(PropertyName = "IsLeft")]
        public bool IsLeft { get; set; }

        [JsonProperty(PropertyName = "IsRight")]
        public bool IsRight { get; set; }

        [JsonProperty(PropertyName = "SoundGroups")]
        public IQueryable<SoundGroup> SoundGroups { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;
using dtos = Diforb.DAL.Classes;

namespace Diforb.WebApi.DTOs.ClientApp
{
    public class Category
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "IconClass")]
        public string IconClass { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Order")]
        public int Order { get; set; }

        [JsonProperty(PropertyName = "Subcategories")]
        public IQueryable<Category> SubCategory { get; set; }

        [JsonProperty(PropertyName = "Sounds")]
        public IQueryable<Sound> Sounds { get; set; }

        public static implicit operator Category(dtos.Category i)
        {
            var category = new Category
            {
                Id = i.Id,
                IconClass = i.Icon,
                Title = i.Title,
                Order = i.Order
            };

            if (i.Categories != null &&
                i.Categories.Any())
            {
                category.SubCategory = i.Categories.Select(c => (Category)c).AsQueryable();
            }

            if (i.Sounds == null || !i.Sounds.Any()) return category;

            category.Sounds = i.Sounds.Select(s => (Sound)s).AsQueryable();

            return category;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;
using dtos = Diforb.DAL.Classes;

namespace Diforb.WebApi.DTOs.ClientApp
{
    public class SoundGroup
    {
        [JsonProperty(PropertyName = "CategoryClass")]
        public string CategoryClass { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "categories")]
        public IQueryable<Category> Categories { get; set; }

        public static implicit operator SoundGroup(dtos.SoundGroup i)
        {
            var soundGroup = new SoundGroup
            {
                Name = i.Name,
                CategoryClass = i.Icon
            };

            if (i.Categories != null &&
                i.Categories.Any())
            {
                soundGroup.Categories = i.Categories.Select(c => (Category)c).AsQueryable();
            }

            if (i.Categories == null || !i.Categories.Any()) return soundGroup;
            soundGroup.Categories = i.Categories.ToArray().Select(l => (Category)l).AsQueryable();

            return soundGroup;
        }

    }
}
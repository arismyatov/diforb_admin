﻿using System;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class LibraryDto
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "Order")]
        public int Order { get; set; }

        public static implicit operator LibraryDto(Library i)
        {
            return new LibraryDto
            {
                Id = i.Id,
                Title = i.Title,
                Order = i.Order
            };
        }
    }
}
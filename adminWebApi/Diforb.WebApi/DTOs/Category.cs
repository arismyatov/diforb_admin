﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class CategoryDto
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Icon")]
        public string Icon { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "Order")]
        public int Order { get; set; }

        [JsonProperty(PropertyName = "Library")]
        public string Library { get; set; }

        [JsonProperty(PropertyName = "LibraryId")]
        public Guid LibraryId { get; set; }

        [JsonProperty(PropertyName = "Parent")]
        public string Parent { get; set; }

        [JsonProperty(PropertyName = "ParentCategoryId")]
        public Guid? ParentCategoryId { get; set; }

        [JsonProperty(PropertyName = "FullTitle")]
        public string FullTitle { get; set; }

        [JsonProperty(PropertyName = "SoundGroups")]
        public List<SoundGroupDto> SoundGroups { get; set; }

        public static implicit operator CategoryDto(Category i)
        {
            var categoryDto = new CategoryDto
            {
                Id      = i.Id,
                Name    = i.Name,
                Title   = i.Title,
                Description   = i.Description,
                Order = i.Order,
                Icon = i.Icon,
                Library = i.Library != null ? i.Library.Name : string.Empty,
                LibraryId = i.LibraryId,
                Parent  = i.ParentCategory != null ? i.ParentCategory.Name : string.Empty,
                ParentCategoryId = i.ParentCategoryId,
                FullTitle = GetFullTitle(i)
            };

            if (i.SoundGroups == null ||
                !i.SoundGroups.Any())
            {
                return categoryDto;

            }

            categoryDto.SoundGroups = i.SoundGroups.ToArray().Select(l => (SoundGroupDto) l).ToList();
            return categoryDto;
        }

        private static string GetFullTitle(Category category)
        {
            if (category.ParentCategory == null) return category.Title;
            var name = new StringBuilder();
            name.Append(GetFullTitle(category.ParentCategory));
            name.Append(" - ");
            name.Append(category.Title);
            return name.ToString();
        }
    }
}
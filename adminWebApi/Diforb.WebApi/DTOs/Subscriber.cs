﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class SubscriberDto
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        public static implicit operator SubscriberDto(Subscriber i)
        {
            return new SubscriberDto
            {
                Id      = i.Id,
                Email   = i.Email
            };
        }
    }
}
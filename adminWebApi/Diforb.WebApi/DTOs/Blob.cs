﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class BlobDto
    {
        public Guid CategoryId { get; set; }

        public Guid SoundId { get; set; }
    }
}
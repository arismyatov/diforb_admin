﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diforb.DAL.Classes;
using Newtonsoft.Json;

namespace Diforb.WebApi.DTOs
{
    public class SoundDto
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "Icon")]
        public string Icon { get; set; }

        [JsonProperty(PropertyName = "Order")]
        public int Order { get; set; }

        [JsonProperty(PropertyName = "Library")]
        public string Library { get; set; }

        [JsonProperty(PropertyName = "LibraryId")]
        public Guid LibraryId { get; set; }

        [JsonProperty(PropertyName = "Categories")]
        public List<CategoryDto> Categories { get; set; }

        public static implicit operator SoundDto(Sound i)
        {
            var soundDto = new SoundDto
            {
                Id = i.Id,
                Name = i.Name,
                Title = i.Title,
                Description = i.Description,
                Icon = i.Icon,
                Order = i.Order
            };

            if (i.Categories == null || !i.Categories.Any()) return soundDto;
            soundDto.Categories = i.Categories.ToArray().Select(l => (CategoryDto)l).ToList();

            var firstCategory = i.Categories.First();
            if (firstCategory == null)
            {
                return soundDto;
            }
            soundDto.LibraryId = firstCategory.LibraryId;

            return soundDto;
        }
    }
}
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:template match="/">
      <p>
        Благодарим за подписку! <br/>
        О всех наших новостях Вы узнаете первыми! <br/>
      </p>
      С уважением, команда Diforb.
    </xsl:template>
</xsl:stylesheet>
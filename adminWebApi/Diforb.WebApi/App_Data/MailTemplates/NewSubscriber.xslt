﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:template match="/">
      There is a new request for subscription on diforb news. Email: <br/>
      <xsl:value-of select="//Email"/>,<br/>
    </xsl:template>
</xsl:stylesheet>